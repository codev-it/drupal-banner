<?php

/**
 * @file
 * Hooks provided by the Codev banner module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform alterations the slick slider banner settings before rendering.
 *
 * In addition to hook_banner_options_alter(), it is possible to hook via
 * MACHINE_NAME, BUNDLE or VIEW_MODE.
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @see          hook_banner_options_MACHINE_NAME_alter
 * @see          hook_banner_options_BUNDLE_alter
 * @see          hook_banner_options_VIEW_MODE_alter
 * @see          hook_banner_options_MACHINE_NAME__VIEW_MODE_alter
 * @see          hook_banner_options_BUNDLE__VIEW_MODE_alter
 * @see          hook_banner_options_MACHINE_NAME__BUNDLE__VIEW_MODE_alter
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (MACHINE_NAME)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_MACHINE_NAME_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (BUNDLE)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_BUNDLE_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (VIEW_MODE)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_VIEW_MODE_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (MACHINE_NAME - VIEW_MODE)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_MACHINE_NAME__VIEW_MODE_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (BUNDLE - VIEW_MODE)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_BUNDLE__VIEW_MODE_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * Perform alterations the slick slider banner settings before rendering.
 * (MACHINE_NAME - BUNDLE - VIEW_MODE)
 *
 * @param array $variables
 *
 * @ingroup      codev_banner
 *
 * @noinspection PhpUnused
 */
function hook_banner_options_MACHINE_NAME__BUNDLE__VIEW_MODE_alter(array &$variables): void {
  $variables['autoplay'] = FALSE;
}

/**
 * @} End of "addtogroup hooks".
 */
