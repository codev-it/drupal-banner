// noinspection All

(function ($, Drupal, once) {
  var behaviors = Drupal.behaviors;

  /**
   * Banner builder svg resizer.
   *
   * @param {HTMLElement} context current context
   */
  Drupal.bannerBuilderSVGResizer = function (context) {
    var $elems = $(context).find('[data-banner-builder-svg]');
    $elems.each(function (i) {
      var $elem = $elems.eq(i);
      var $children = $elem.children();
      if ($children.length > 1) {
        $children.each(function (elemI) {
          var $item = $children.eq(elemI);
          var $nextItem = $children.eq(elemI + 1);
          var breakpoint = $item.data('banner-builder-svg-breakpoint');
          var mediaQuery = [];
          if (breakpoint) {
            mediaQuery.push("(min-width: ".concat(breakpoint, ")"));
          }
          if ($nextItem.length) {
            var nextBreakpoint = $nextItem.data('banner-builder-svg-breakpoint');
            nextBreakpoint && mediaQuery.push("(max-width: ".concat(nextBreakpoint, ")"));
          }
          $item.hide();
          if (window.matchMedia(mediaQuery.join(' and ')).matches) {
            $item.show();
          }
        });
      }
    });
  };

  /**
   * Banner builder svg resizer behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevBannerBuilderSvg = {
    attach: function attach(context) {
      if (!once('banner-builder-svg-resizer', $(context)).length) {
        return;
      }
      var $window = $(window);
      Drupal.bannerBuilderSVGResizer(context);
      $window.resize(function () {
        Drupal.bannerBuilderSVGResizer(context);
      });
    }
  };
})(jQuery, Drupal, once);
