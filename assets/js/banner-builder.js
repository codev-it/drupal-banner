// noinspection All

function _toConsumableArray(r) { return _arrayWithoutHoles(r) || _iterableToArray(r) || _unsupportedIterableToArray(r) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _iterableToArray(r) { if ("undefined" != typeof Symbol && null != r[Symbol.iterator] || null != r["@@iterator"]) return Array.from(r); }
function _arrayWithoutHoles(r) { if (Array.isArray(r)) return _arrayLikeToArray(r); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _slicedToArray(r, e) { return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray(r, e) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _iterableToArrayLimit(r, l) { var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (null != t) { var e, n, i, u, a = [], f = !0, o = !1; try { if (i = (t = t.call(r)).next, 0 === l) { if (Object(t) !== t) return; f = !1; } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0); } catch (r) { o = !0, n = r; } finally { try { if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return; } finally { if (o) throw n; } } return a; } }
function _arrayWithHoles(r) { if (Array.isArray(r)) return r; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
// noinspection DuplicatedCode

(function ($, Drupal, fabric) {
  var editors = Drupal.editors,
    dialog = Drupal.dialog,
    t = Drupal.t;

  /**
   * Banner builder storage handler.
   *
   * @param {HTMLTextAreaElement} element persist storage element
   * @param {Object} format editor format settings
   *
   * @constructor
   */
  function Storage(element, format) {
    /**
     * Element object.
     *
     * @type {HTMLTextAreaElement}
     */
    this.element = element;

    /**
     * Formate object.
     *
     * @type {Object}
     */
    this.formate = format;

    /**
     * Settings object.
     *
     * @type {HTMLDivElement}
     */
    this.settings = _cloneObj(BannerBuilder.defSettings);

    /**
     * Container object.
     *
     * @type {HTMLDivElement}
     */
    this.container = document.createElement('div');

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvas = {};

    /**
     * List off all available fonts.
     *
     * @type {Array}
     */
    this.fonts = _availableFonts(format);

    /**
     * Fullscreen status.
     *
     * @type {boolean}
     */
    this.fullscreen = false;

    // merge format editor settings
    var editorSettings = format.editorSettings;
    var devices = editorSettings.devices;
    _typeof(devices) === 'object' && _mergeObj(this.settings.devices, devices);

    // load from persist element storage
    this.load();
  }

  /**
   * Load persist data from element and update storage.
   */
  Storage.prototype.load = function () {
    var _this = this;
    var value = this.element.value;
    if (value && value.charAt(0) === '{') {
      var _JSON$parse = JSON.parse(value),
        settings = _JSON$parse.settings,
        canvas = _JSON$parse.canvas;
      settings && _mergeObj(this.settings, settings);
      Object.entries(canvas || {}).forEach(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 1),
          key = _ref2[0];
        if (!_this.canvas[key] && canvas[key]) {
          _this.canvas[key] = canvas[key];
        }
      });
    }
  };

  /**
   * Save the current settings and infos to element.
   */
  Storage.prototype.save = function () {
    var svg = {};
    Object.entries(this.canvas).forEach(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
        key = _ref4[0],
        canvas = _ref4[1];
      svg[key] = canvas.toSVG();
    });
    this.element.setAttribute('data-editor-value-is-changed', 'true');
    this.element.value = JSON.stringify({
      settings: _removePrivatePropNew(this.settings),
      canvas: this.canvas,
      svg: svg
    });
  };

  /**
   * Get settings by keys.
   *
   * @param {string|array} key settings key
   * @param {*} fallback fallback
   *
   * @return {*} Settings object
   */
  Storage.prototype.getSetting = function (key) {
    var fallback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var scope = this.settings;
    switch (_typeof(key)) {
      case 'string':
        return scope[key] || fallback;
      case 'object':
        if (!key.length) {
          return fallback;
        }
        key.forEach(function (val) {
          if (_typeof(scope) === 'object' && scope[val] !== undefined) {
            scope = scope[val];
          } else {
            scope = fallback;
          }
        });
        return scope;
      default:
        return fallback;
    }
  };

  /**
   * Set settings by keys.
   *
   * @param {string|array} key settings key
   * @param {*} val settings values
   */
  Storage.prototype.setSetting = function (key, val) {
    var scope = this.settings;
    var max = key.length;
    switch (_typeof(key)) {
      case 'string':
        scope[key] = val;
        break;
      case 'object':
        if (max) {
          key.forEach(function (k, index) {
            if (scope[k] !== undefined) {
              if (index === max - 1) {
                scope[k] = val;
              } else {
                scope = scope[k];
              }
            } else {
              scope[k] = index === max - 1 ? val : {};
              scope = scope[k];
            }
          });
        }
        break;
      default:
    }
  };

  /**
   * Banner builder device handler.
   *
   * @param {Object} devices default devices settings
   *
   * @constructor
   */
  function Devices(devices) {
    /**
     * Devices list object.
     *
     * @type {Object}
     */
    this.list = devices;

    /**
     * Devices list entities.
     *
     * @type {[string, unknown][]}
     */
    this.entries = Object.entries(this.list);

    // eslint-disable-next-line prefer-destructuring
    /**
     * Current device object.
     *
     * @type {Object}
     */
    this.current = this.last();
  }

  /**
   * Iterate device entities.
   *
   * @param {function} fn for each element function
   */
  Devices.prototype.forEach = function (fn) {
    if (typeof fn === 'function') {
      this.entries.forEach(function (_ref5, index) {
        var _ref6 = _slicedToArray(_ref5, 2),
          name = _ref6[0],
          data = _ref6[1];
        fn(_objectSpread({
          name: name
        }, data || {}), index);
      });
    }
  };

  /**
   * Find device data by name.
   *
   * @param {string} deviceName device name
   *
   * @return {Object|false} device
   *   object
   */
  Devices.prototype.find = function (deviceName) {
    if (this.list[deviceName] !== undefined) {
      return _objectSpread({
        name: deviceName
      }, this.list[deviceName]);
    }
    return false;
  };

  /**
   * Get first device element.
   *
   * @return {Object} device object
   */
  Devices.prototype.first = function () {
    var _this$entries$at = this.entries.at(0),
      _this$entries$at2 = _slicedToArray(_this$entries$at, 2),
      name = _this$entries$at2[0],
      data = _this$entries$at2[1];
    return _objectSpread({
      name: name
    }, data);
  };

  /**
   * Get last device element.
   *
   * @return {Object} device object
   */
  Devices.prototype.last = function () {
    var _this$entries$at3 = this.entries.at(-1),
      _this$entries$at4 = _slicedToArray(_this$entries$at3, 2),
      name = _this$entries$at4[0],
      data = _this$entries$at4[1];
    return _objectSpread({
      name: name
    }, data);
  };

  /**
   * Get next device element.
   *
   * @param {string} deviceName current device
   *
   * @return {Object} device object
   */
  Devices.prototype.next = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return this._sibling(deviceName);
  };

  /**
   * Get preview device element.
   *
   * @param {string} deviceName current device
   *
   * @return {Object} device object
   */
  Devices.prototype.prev = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return this._sibling(deviceName, 'prev');
  };

  /**
   * Get device sibling by current device name.
   *
   * @param {string} deviceName current device
   * @param {string} direct direction
   *
   * @return {Object} device object
   *
   * @private
   */
  Devices.prototype._sibling = function () {
    var _this2 = this;
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var direct = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'next';
    var maxIndex = this.entries.length - 1;
    var current = this.find(deviceName) || this.current;
    var sibling = current;
    this.forEach(function (_ref7, i) {
      var name = _ref7.name;
      var newIndex = direct === 'next' ? i + 1 : i - 1;
      if (name === current.name && newIndex >= 0 && newIndex <= maxIndex) {
        var _this2$entries$at = _this2.entries.at(newIndex),
          _this2$entries$at2 = _slicedToArray(_this2$entries$at, 2),
          key = _this2$entries$at2[0],
          val = _this2$entries$at2[1];
        sibling = _objectSpread({
          key: key
        }, val || {});
      }
    });
    return sibling;
  };

  /**
   * Change current device.
   *
   * @param {string} deviceName current device
   *
   * @return {boolean} device object
   */
  Devices.prototype.change = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    if (this.current.name !== deviceName) {
      var newDevice = this.find(deviceName);
      if (newDevice) {
        this.current = newDevice;
        return true;
      }
    }
    return false;
  };

  /**
   * Banner builder ui markup handler.
   *
   * @param {BannerBuilder} self banner builder object
   *
   * @constructor
   */
  function Markup(self) {
    var storage = self.storage,
      devices = self.devices;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;
  }

  /**
   * Create button element.
   *
   * @param {{name: string, value: string, icon: Array, classes: Array,
   *   data: Object}} object button name
   *
   * @return {HTMLSpanElement} button element
   */
  Markup.prototype.button = function (_ref8) {
    var _elem$classList;
    var title = _ref8.title,
      value = _ref8.value,
      icon = _ref8.icon,
      classes = _ref8.classes,
      data = _ref8.data;
    var elem = document.createElement('span');
    elem.textContent = value || '';
    (_elem$classList = elem.classList).add.apply(_elem$classList, _toConsumableArray(classes || []));
    if (title) {
      title = t(title);
      elem.setAttribute('title', title);
      elem.setAttribute('alt', title);
    }
    if (Object.keys(icon || []).length) {
      var _iconElem$classList;
      var iconElem = document.createElement('i');
      (_iconElem$classList = iconElem.classList).add.apply(_iconElem$classList, _toConsumableArray(icon));
      elem.appendChild(iconElem);
    }
    Object.entries(data || {}).forEach(function (_ref9) {
      var _ref10 = _slicedToArray(_ref9, 2),
        key = _ref10[0],
        val = _ref10[1];
      elem.dataset[key] = val.toString();
    });
    return elem;
  };

  /**
   * Create button group element.
   *
   * @param {array} arr buttons
   *
   * @return {HTMLDivElement} buttons element
   */
  Markup.prototype.buttons = function (arr) {
    var _this3 = this;
    var elem = document.createElement('div');
    elem.classList.add('button-group');
    arr.forEach(function (data) {
      elem.appendChild(_this3.button(data));
    });
    return elem;
  };

  /**
   * Create a tabs element.
   *
   * @param {Object} data data object
   *
   * @return {{tabsContent: {}, tabs: HTMLDivElement}} tabs object
   */
  Markup.prototype.tabs = function (data) {
    var tabs = document.createElement('div');
    var header = document.createElement('ul');
    var content = document.createElement('div');
    var tabsContent = {};
    tabs.classList.add('tabs-cn');
    header.classList.add('tabs');
    content.classList.add('tabs-content');
    var index = 0;
    Object.entries(data).forEach(function (_ref11) {
      var _ref12 = _slicedToArray(_ref11, 2),
        key = _ref12[0],
        item = _ref12[1];
      if (key[0] !== '_') {
        var _button = item._button;
        var _isActive = item._isActive;
        var isActiveClass = 'is-active';
        var itemId = "tab-content-".concat(key, "-").concat(_randStr());
        var headerItem = document.createElement('li');
        var contentItem = document.createElement('div');
        _isActive = _isActive || index === 0;
        headerItem.textContent = _button ? t(_button) : key;
        headerItem.classList.add('tab-title');
        headerItem.addEventListener('click', function (_ref13) {
          var target = _ref13.target;
          if (!target.classList.contains(isActiveClass)) {
            var activeTitle = target.parentNode.querySelector('.is-active');
            target.classList.add(isActiveClass);
            activeTitle && activeTitle.classList.remove(isActiveClass);
            Object.entries(content.children).forEach(function (_ref14) {
              var _ref15 = _slicedToArray(_ref14, 2),
                entry = _ref15[1];
              if (entry.id === itemId) {
                entry.classList.add(isActiveClass);
              } else {
                entry.classList.remove(isActiveClass);
              }
            });
          }
        });
        contentItem.id = itemId;
        contentItem.classList.add('tab-panel');
        if (_isActive) {
          headerItem.classList.add(isActiveClass);
          contentItem.classList.add(isActiveClass);
        }
        tabsContent[key] = contentItem;
        header.appendChild(headerItem);
        content.appendChild(contentItem);
        index += 1;
      }
    });
    tabs.appendChild(header);
    tabs.appendChild(content);
    return {
      tabs: tabs,
      tabsContent: tabsContent
    };
  };

  /**
   * Create form input field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLInputElement} input field markup
   */
  Markup.prototype.formInputField = function (_ref16) {
    var _type = _ref16._type,
      _name = _ref16._name,
      _value = _ref16._value,
      _defaultValue = _ref16._defaultValue,
      _fieldType = _ref16._fieldType,
      _accept = _ref16._accept,
      _required = _ref16._required;
    _name = _name || '';
    _value = _value || '';
    _defaultValue = _defaultValue || '';
    _fieldType = _fieldType || 'input';
    var elem = document.createElement(_fieldType);
    var valueKeys = _value.split('.') || [];
    var fieldId = valueKeys.join('-');
    var fieldName = _buildFormName(valueKeys) || _name;
    var settingVal = this.storage.getSetting(valueKeys);
    var fieldVal = settingVal !== undefined ? settingVal : _defaultValue;
    fieldVal = fieldVal !== undefined ? fieldVal : _value;
    elem.id = fieldId;
    elem.setAttribute('name', fieldName);
    elem.type = _type || 'text';
    elem.value = fieldVal;
    elem.classList.add("field-".concat(_fieldType));
    _required && elem.setAttribute('required', 'true');
    _accept && elem.setAttribute('accept', _accept.join(', '));
    return elem;
  };

  /**
   * Create form select field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLSelectElement} select field markup
   */
  Markup.prototype.formSelectField = function (_ref17) {
    var _type = _ref17._type,
      _name = _ref17._name,
      _value = _ref17._value,
      _defaultValue = _ref17._defaultValue,
      _options = _ref17._options,
      _required = _ref17._required;
    _name = _name || '';
    _value = _value || '';
    _options = _options || {};
    _defaultValue = _defaultValue || '';
    var valueKeys = _value.split('.') || [];
    var fieldId = valueKeys.join('-');
    var fieldName = _buildFormName(valueKeys) || _name;
    var settingVal = this.storage.getSetting(valueKeys);
    var fieldVal = settingVal !== undefined ? settingVal : _defaultValue;
    fieldVal = fieldVal !== undefined ? fieldVal : _value;
    var isOptionArray = Array.isArray(_options);
    var elem = document.createElement('select');
    elem.id = fieldId;
    elem.setAttribute('name', fieldName);
    elem.classList.add("field-select");
    _required && elem.setAttribute('required', 'true');
    Object.entries(_options).forEach(function (_ref18) {
      var _ref19 = _slicedToArray(_ref18, 2),
        key = _ref19[0],
        val = _ref19[1];
      var option = document.createElement('option');
      var value = isOptionArray ? val : key;
      if (_typeof(value) === 'object') {
        value = value.textContent;
      }
      option.value = value.toString();
      if (value.trim() === fieldVal.trim()) {
        option.setAttribute('selected', 'true');
      }
      if (val instanceof HTMLElement) {
        option.appendChild(val);
      } else if (val) {
        option.textContent = val.toString();
      }
      elem.appendChild(option);
    });
    return elem;
  };

  /**
   * Create form field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLDivElement} field markup
   */
  Markup.prototype.formField = function (_ref20) {
    var _elem$classList2;
    var _type = _ref20._type,
      _name = _ref20._name,
      _label = _ref20._label,
      _value = _ref20._value,
      _defaultValue = _ref20._defaultValue,
      _description = _ref20._description,
      _fieldType = _ref20._fieldType,
      _options = _ref20._options,
      _classes = _ref20._classes,
      _accept = _ref20._accept,
      _required = _ref20._required;
    _name = _name || '';
    _value = _value || '';
    _fieldType = _fieldType || 'input';
    var elem = document.createElement('div');
    var label = _label !== undefined && _label !== '' ? t(_label) : '';
    var valueKeys = _value.split('.') || [];
    var fieldId = valueKeys.join('-');
    var labelElem = document.createElement('label');
    (_elem$classList2 = elem.classList).add.apply(_elem$classList2, ['field', "field-type-".concat(_type)].concat(_toConsumableArray(_classes || [])));
    labelElem.textContent = label;
    labelElem.setAttribute('for', fieldId);
    labelElem.classList.add('field-label');
    _required && labelElem.classList.add('form-required');
    elem.appendChild(labelElem);
    switch (_type) {
      case 'select':
        elem.appendChild(this.formSelectField({
          _type: _type,
          _name: _name,
          _value: _value,
          _defaultValue: _defaultValue,
          _options: _options,
          _required: _required
        }));
        break;
      default:
        elem.appendChild(this.formInputField({
          _type: _type,
          _name: _name,
          _value: _value,
          _defaultValue: _defaultValue,
          _fieldType: _fieldType,
          _accept: _accept,
          _required: _required
        }));
    }
    if (_description) {
      var descriptionElem = document.createElement('div');
      descriptionElem.textContent = _description;
      descriptionElem.classList.add('field-description');
      elem.appendChild(descriptionElem);
    }
    return elem;
  };

  /**
   * Build recursively elements.
   *
   * @param {Object} data data object
   *
   * @return {HTMLDivElement} markup
   */
  Markup.prototype.buildFormElements = function (data) {
    var _elem$classList3,
      _this4 = this;
    var _type = data._type,
      _label = data._label,
      _tag = data._tag,
      _classes = data._classes;
    var elem = document.createElement('div');
    var belowItems = Object.entries(_removePrivatePropNew(data));
    var belowElemRef = {};
    elem.classList.add('field-container');
    var switchVar;
    switch (_type) {
      case 'tabs':
        switchVar = this.tabs(data);
        belowElemRef = switchVar.tabsContent;
        elem = switchVar.tabs;
        break;
      case 'html_tag':
        elem = document.createElement(_tag || 'p');
        elem.textContent = _label || '';
        (_elem$classList3 = elem.classList).add.apply(_elem$classList3, _toConsumableArray(_classes || []));
        break;
      case 'textfield':
        elem = this.formField(_objectSpread(_objectSpread({}, data), {}, {
          _type: 'text'
        }));
        break;
      case 'select':
        elem = this.formField(_objectSpread(_objectSpread({}, data), {}, {
          _type: 'select'
        }));
        break;
      case 'color':
        elem = this.formField(_objectSpread(_objectSpread({}, data), {}, {
          _type: 'color'
        }));
        break;
      case 'file':
        elem = this.formField(_objectSpread(_objectSpread({}, data), {}, {
          _type: 'file'
        }));
        break;
      default:
        if (_label) {
          var _switchVar$classList;
          switchVar = document.createElement('span');
          switchVar.textContent = t(_label);
          (_switchVar$classList = switchVar.classList).add.apply(_switchVar$classList, ['title'].concat(_toConsumableArray(_classes || [])));
          elem.appendChild(switchVar);
        }
    }
    if (belowItems.length) {
      belowItems.forEach(function (_ref21) {
        var _ref22 = _slicedToArray(_ref21, 1),
          key = _ref22[0];
        var belowElem = belowElemRef[key] || elem;
        belowElem.appendChild(_this4.buildFormElements(data[key]));
      });
    }
    return elem;
  };

  /**
   * Create form elements.
   *
   * @param {string} id form id
   * @param {Object} data data object
   *
   * @return {{}} form
   *   element
   */
  Markup.prototype.form = function (id) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var error = {};
    var form = document.createElement('form');
    form.id = id;
    form.classList.add('banner-builder-form');
    form.appendChild(this.buildFormElements(data || {}));
    return {
      form: form,
      /**
       * Return form data object.
       *
       * @return {FormData} form data
       */
      formData: function formData() {
        return new FormData(form);
      },
      /**
       * Validate form data.
       *
       * @return {boolean} validation status
       */
      formValide: function formValide() {
        var _this5 = this;
        var valide = true;
        var formData = this.formData();
        var _getFieldsToValidate = function getFieldsToValidate(elem) {
          var arr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
          var below = _removePrivatePropNew(elem);
          if (elem._required || elem._accept || elem._validate) {
            arr.push(elem);
          }
          if (below) {
            Object.entries(below).forEach(function (_ref23) {
              var _ref24 = _slicedToArray(_ref23, 1),
                key = _ref24[0];
              _getFieldsToValidate(elem[key], arr);
            });
          }
          return arr;
        };
        Object.entries(_getFieldsToValidate(data)).forEach(function (_ref25) {
          var _ref26 = _slicedToArray(_ref25, 2),
            _ref26$ = _ref26[1],
            _type = _ref26$._type,
            _name = _ref26$._name,
            _value = _ref26$._value,
            _required = _ref26$._required,
            _accept = _ref26$._accept,
            _validate = _ref26$._validate;
          var fieldName = _name;
          if (_value) {
            fieldName = _buildFormName(_value.split('.') || []);
          }
          var value = formData.get(fieldName);
          if (_required) {
            var showError = false;
            switch (_type) {
              case 'file':
                if (!value.size) {
                  showError = true;
                }
                break;
              default:
                if (!value) {
                  showError = true;
                }
            }
            if (showError) {
              _this5.setFormError(fieldName, 'required', t('Field is required.'));
              valide = false;
            } else {
              _this5.removeFormError(fieldName, 'required');
            }
          }
          if (_accept) {
            if (value.size && !_accept.includes(value.type)) {
              _this5.setFormError(fieldName, 'accept', t('File has wrong MIME type.'));
              valide = false;
            } else {
              _this5.removeFormError(fieldName, 'accept');
            }
          }
        });
        return valide;
      },
      /**
       * Set form error.
       *
       * @param {string} name field name
       * @param {string} type type error
       * @param {string} msg error message
       */
      setFormError: function setFormError(name) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
        var msg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
        var key = type !== '' ? "".concat(name, "-").concat(type) : name;
        var field = form.querySelector("[name=\"".concat(name, "\"]"));
        if (field && !error[key]) {
          var wrp = field.parentNode;
          var elem = document.createElement('div');
          elem.classList.add('form-item--error-message');
          elem.textContent = msg;
          wrp.appendChild(elem);
          error[key] = elem;
        }
      },
      /**
       * Remove form error.
       *
       * @param {string} name field name
       * @param {string} type type error
       */
      removeFormError: function removeFormError(name) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
        var key = type !== '' ? "".concat(name, "-").concat(type) : name;
        if (error[key]) {
          error[key].remove();
          delete error[key];
        }
      }
    };
  };

  /**
   * Polyfill HTML5 dialog element with jQueryUI.
   *
   * @param {HTMLElement} element
   *   The element that holds the dialog.
   * @param {Object} options
   *   jQuery UI options to be passed to the dialog.
   *
   * @return {Drupal.dialog~dialogDefinition}
   *   The dialog instances.
   */
  Markup.prototype.dialog = function (element) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var wrp = document.createElement('div');
    wrp.appendChild(element);
    return dialog(wrp, options);
  };

  /**
   * Banner builder toolbar handler.
   *
   * @param {BannerBuilder} self banner builder object
   *
   * @constructor
   */
  function Toolbar(self) {
    var storage = self.storage,
      devices = self.devices,
      markup = self.markup,
      drawing = self.drawing;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;

    /**
     * Markup handler.
     *
     * @type {BannerBuilder.markup|Markup}
     */
    this.markup = markup;

    /**
     * Markup drawing.
     *
     * @type {BannerBuilder.drawing|Drawing}
     */
    this.drawing = drawing;

    /**
     * Toolbar container element.
     *
     * @type {HTMLDivElement}
     */
    this.toolbar = document.createElement('div');

    /**
     * Toolbar middle element.
     *
     * @type {HTMLDivElement}
     */
    this.toolbarMiddle = document.createElement('div');
  }

  /**
   * Toolbar elements data.
   *
   * @return {Object} toolbar data object
   */
  Toolbar.prototype.toolbarElements = function () {
    var toolbar = Toolbar.toolbarElements;
    var editorSettings = this.storage.formate.editorSettings;
    var features = editorSettings.features;

    /**
     * Remove disabled elements.
     *
     * @param {Object} elements elements object to formate
     * @param {Object} data element status data object
     */
    var removeDisabledElements = function removeDisabledElements(elements, data) {
      Object.entries(data).forEach(function (_ref27) {
        var _ref28 = _slicedToArray(_ref27, 2),
          key = _ref28[0],
          enable = _ref28[1];
        if (!enable && elements[key] !== undefined) {
          delete elements[key];
        }
      });
    };
    removeDisabledElements(toolbar.left, features);
    removeDisabledElements(toolbar.right, features);
    return toolbar;
  };

  /**
   * Render toolbar elements.
   *
   * @param {Object} data toolbar data
   *
   * @return {HTMLDivElement} toolbar element
   */
  Toolbar.prototype.buildElements = function (data) {
    var _this6 = this;
    var devices = this.devices,
      markup = this.markup;
    var toolbar = document.createElement('div');
    var currentDevice = devices.current;
    Object.entries(data).forEach(function (_ref29) {
      var _ref30 = _slicedToArray(_ref29, 2),
        key = _ref30[0],
        value = _ref30[1];
      var elem;
      var switchVar;
      var type = value.type,
        title = value.title,
        icon = value.icon,
        options = value.options,
        defaultValue = value.defaultValue,
        click = value.click,
        change = value.change;
      switch (type) {
        case 'devicesChanger':
          switchVar = [];
          devices.forEach(function (device) {
            switchVar.push({
              value: device.name,
              classes: currentDevice.name === device.name ? ['toolbar-button', 'is-active'] : ['toolbar-button'],
              data: {
                device: device.name
              }
            });
          });
          elem = markup.buttons(switchVar);
          elem.classList.add('device-changer');
          break;
        case 'select':
          elem = markup.formField({
            _type: 'select',
            _label: title,
            _name: key,
            _options: options,
            _defaultValue: defaultValue
          });
          break;
        case 'spacer':
          elem = document.createElement('span');
          elem.classList.add('toolbar-spacer');
          break;
        default:
          elem = markup.button({
            title: title,
            icon: icon,
            classes: ['toolbar-button']
          });
      }
      if (click) {
        elem.addEventListener('click', click.bind(_this6));
      }
      if (change) {
        elem.addEventListener('change', change.bind(_this6));
      }
      elem && toolbar.appendChild(elem);
    });
    return toolbar;
  };

  /**
   * Build the toolbar markup.
   *
   * @returns {HTMLDivElement} toolbar element
   */
  Toolbar.prototype.build = function () {
    var _this$toolbarElements = this.toolbarElements(),
      left = _this$toolbarElements.left,
      right = _this$toolbarElements.right;
    var toolbarLeft = this.buildElements(left);
    var toolbarRight = this.buildElements(right);
    this.toolbar.classList.add('toolbar-wrp', 'row');
    toolbarLeft.classList.add('left', 'column');
    this.toolbarMiddle.classList.add('middle', 'column');
    toolbarRight.classList.add('right', 'column');
    this.toolbar.appendChild(toolbarLeft);
    this.toolbar.appendChild(this.toolbarMiddle);
    this.toolbar.appendChild(toolbarRight);
    return this.toolbar;
  };

  /**
   * Toolbar elements data.
   *
   * @type {Object}
   */
  Toolbar.toolbarElements = {
    left: {
      backgroundImage: {
        title: 'Background image',
        icon: ['fas', 'fa-images'],
        align: 'left',
        buildFormElements: function buildFormElements(name) {
          return {
            color: {
              _type: 'color',
              _name: 'color',
              _label: 'Background color',
              _description: 'Select background color',
              _value: "backgroundImage.".concat(name, ".color"),
              _defaultValue: '#ffffff'
            },
            file: {
              _type: 'file',
              _name: 'file',
              _label: 'Background image',
              _description: 'Select image to upload. Accept: PNG, JPG, GIF',
              _accept: ['image/png', 'image/jpeg', 'image/gif']
            },
            scale: {
              _type: 'select',
              _name: 'scale',
              _label: 'Background image select',
              _description: 'Select background image scale',
              _value: "backgroundImage.".concat(name, ".scale"),
              _options: ['cover', 'contain']
            },
            position: {
              _type: 'select',
              _name: 'scale',
              _label: 'Background image position',
              _description: 'Select background image position',
              _value: "backgroundImage.".concat(name, ".position"),
              _defaultValue: 'center-center',
              _options: {
                'top-left': t('Top left'),
                'top-center': t('Top center'),
                'top-right': t('Top right'),
                'center-left': t('Center left'),
                'center-center': t('Center center'),
                'center-right': t('Center right'),
                'bottom-left': t('Bottom left'),
                'bottom-center': t('Bottom center'),
                'bottom-right': t('Bottom right')
              }
            }
          };
        },
        click: function click() {
          var storage = this.storage,
            devices = this.devices,
            markup = this.markup,
            drawing = this.drawing;
          var currentDevice = devices.current;
          var scopeFn = Toolbar.toolbarElements.left.backgroundImage;
          var formElements = scopeFn.buildFormElements(currentDevice.name);
          var formMarkup = markup.form('background-image-upload-form', formElements);
          var form = formMarkup.form,
            formData = formMarkup.formData;
          var colorFieldNameArr = ['backgroundImage', currentDevice.name, 'color'];
          var colorFieldName = _buildFormName(colorFieldNameArr);
          var scaleFieldName = _buildFormName(['backgroundImage', currentDevice.name, 'scale']);
          var positionFieldName = _buildFormName(['backgroundImage', currentDevice.name, 'position']);
          var backgroundImageUploadDialog = markup.dialog(form, {
            title: t('Background image'),
            buttons: [{
              text: t('Save'),
              "class": 'button button--primary',
              click: function click() {
                if (!formMarkup.formValide()) {
                  return;
                }
                var reader = new FileReader();
                var formDataValues = formData();
                var scale = formDataValues.get(scaleFieldName);
                var position = formDataValues.get(positionFieldName);
                var _position$split = position.split('-'),
                  _position$split2 = _slicedToArray(_position$split, 2),
                  originY = _position$split2[0],
                  originX = _position$split2[1];
                formDataValues.forEach(function (value, name) {
                  var formName = _parseFormName(name);
                  switch (name) {
                    case colorFieldName:
                      drawing.setBackgroundColor(value);
                      storage.setSetting(formName, value);
                      break;
                    case 'file':
                      if (value.size) {
                        reader.onload = function (_ref31) {
                          var target = _ref31.target;
                          drawing.setBackgroundImage(target.result, {
                            scale: scale,
                            originX: originX,
                            originY: originY
                          });
                        };
                        reader.readAsDataURL(value);
                      }
                      break;
                    case scaleFieldName:
                    case positionFieldName:
                      drawing.updateBackgroundImage({
                        scale: scale,
                        originX: originX,
                        originY: originY
                      });
                      storage.setSetting(formName, value);
                      break;
                    default:
                      storage.setSetting(formName, value);
                  }
                });
                drawing.render();
                backgroundImageUploadDialog.close();
              }
            }, {
              text: t('Delete'),
              "class": 'button button--danger',
              click: function click() {
                var colorDefault = formElements.color._defaultValue;
                storage.setSetting(colorFieldNameArr, colorDefault);
                drawing.removeBackgroundColor();
                drawing.removeBackgroundImage();
                drawing.render();
                backgroundImageUploadDialog.close();
              }
            }]
          });
          backgroundImageUploadDialog.showModal();
        }
      },
      image: {
        title: 'Image',
        icon: ['fas', 'fa-image'],
        align: 'left',
        buildFormElements: function buildFormElements() {
          return {
            _type: 'file',
            _name: 'file',
            _label: 'Image',
            _description: 'Select image to upload. Accept: PNG, JPG, GIF',
            _accept: ['image/png', 'image/jpeg', 'image/gif'],
            _required: true
          };
        },
        click: function click() {
          var markup = this.markup,
            drawing = this.drawing;
          var scopeFn = Toolbar.toolbarElements.left.image;
          var formMarkup = markup.form('image-upload-form', scopeFn.buildFormElements());
          var form = formMarkup.form,
            formData = formMarkup.formData;
          var imageUploadDialog = markup.dialog(form, {
            title: t('Image'),
            buttons: [{
              text: t('Save'),
              "class": 'button button--primary',
              click: function click() {
                if (!formMarkup.formValide()) {
                  return;
                }
                var reader = new FileReader();
                var formDataValues = formData();
                var file = formDataValues.get('file');
                if (file.size) {
                  reader.onload = function (_ref32) {
                    var target = _ref32.target;
                    drawing.setImage(target.result);
                  };
                  reader.readAsDataURL(file);
                }
                imageUploadDialog.close();
              }
            }]
          });
          imageUploadDialog.showModal();
        }
      },
      spacer2: {
        type: 'spacer',
        align: 'left'
      },
      textarea: {
        title: 'Textarea',
        icon: ['fas', 'fa-paragraph'],
        align: 'left',
        click: function click() {
          this.drawing.setTextbox('Text');
        }
      }
    },
    right: {
      devices: {
        type: 'devicesChanger',
        title: 'Devices',
        align: 'right',
        weight: -99,
        click: function click(_ref33) {
          var target = _ref33.target;
          var devices = this.devices,
            drawing = this.drawing;
          var device = target.dataset.device;
          var children = target.parentNode.children;
          var isActiveClass = 'is-active';
          if (devices.change(device)) {
            Object.entries(children).forEach(function (_ref34) {
              var _ref35 = _slicedToArray(_ref34, 2),
                elem = _ref35[1];
              elem.classList.remove(isActiveClass);
            });
            target.classList.add(isActiveClass);
            drawing.updateViewports();
          }
        }
      },
      spacer: {
        type: 'spacer',
        align: 'right',
        weight: -98
      },
      fullScreen: {
        title: 'Full screen',
        icon: ['fas', 'fa-expand'],
        align: 'right',
        click: function click() {
          var storage = this.storage,
            drawing = this.drawing;
          var className = 'full-screen';
          var classList = storage.container.classList;
          if (classList.contains(className)) {
            storage.fullscreen = false;
            classList.remove(className);
          } else {
            storage.fullscreen = true;
            classList.add(className);
          }
          drawing.updateViewports();
        }
      },
      settings: {
        title: 'Settings',
        icon: ['fas', 'fa-cog'],
        align: 'right',
        buildDeviceFormElement: function buildDeviceFormElement(name) {
          return {
            _button: _strFirstLetterUc(name),
            breakpoint: {
              _type: 'textfield',
              _label: 'Breakpoint',
              _value: "devices.".concat(name, ".breakpoint")
            },
            height: {
              _type: 'textfield',
              _label: 'Height',
              _value: "devices.".concat(name, ".height"),
              _required: true
            },
            width: {
              _type: 'textfield',
              _label: 'Width',
              _value: "devices.".concat(name, ".width"),
              _required: true
            }
          };
        },
        click: function click() {
          var storage = this.storage,
            devices = this.devices,
            markup = this.markup,
            drawing = this.drawing;
          var settingsForm = BannerBuilder.settingsForm;
          var scopeFn = Toolbar.toolbarElements.right.settings;
          devices.forEach(function (_ref36) {
            var name = _ref36.name;
            settingsForm.devices[name] = scopeFn.buildDeviceFormElement(name);
          });
          var formHandler = markup.form('settings-form', settingsForm);
          var form = formHandler.form,
            formData = formHandler.formData;
          var settingsDialog = markup.dialog(form, {
            title: t('Settings'),
            buttons: [{
              text: t('Save'),
              "class": 'button button--primary',
              click: function click() {
                if (!formHandler.formValide()) {
                  return;
                }
                formData().forEach(function (value, name) {
                  storage.setSetting(_parseFormName(name), value);
                });
                settingsDialog.close();
                drawing.updateViewports();
              }
            }]
          });
          settingsDialog.showModal();
        }
      }
    }
  };

  /**
   * Banner builder drawing handler.
   *
   * @param {Object} self devices handler
   *
   * @constructor
   */
  function Drawing(self) {
    var storage = self.storage,
      devices = self.devices,
      markup = self.markup;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;

    /**
     * Markup handler.
     *
     * @type {BannerBuilder.markup|Markup}
     */
    this.markup = markup;

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvasWrp = document.createElement('div');

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvas = {};
  }

  /**
   * Build the canvas markup.
   *
   * @returns {HTMLDivElement} toolbar element
   */
  Drawing.prototype.build = function () {
    var _this7 = this;
    this.canvasWrp.classList.add('canvas-wrp');
    this.devices.forEach(function (_ref37) {
      var name = _ref37.name;
      var canvasCn = document.createElement('div');
      var canvas = document.createElement('canvas');
      canvasCn.classList.add('canvas-item');
      canvas.id = "canvas-".concat(name, "-").concat(_randStr());
      _this7.canvas[name] = canvas;
      canvasCn.appendChild(canvas);
      _this7.canvasWrp.appendChild(canvasCn);
    });
    return this.canvasWrp;
  };

  /**
   * Build the canvas draw element.
   */
  Drawing.prototype.run = function () {
    var _this8 = this;
    Object.entries(this.canvas).forEach(function (_ref38) {
      var _ref39 = _slicedToArray(_ref38, 2),
        key = _ref39[0],
        elem = _ref39[1];
      var preset = _this8.storage.canvas[key];
      var fabricCanvas = new fabric.Canvas(elem.id);
      preset && preset.objects.length && fabricCanvas.loadFromJSON(preset);
      _this8.storage.canvas[key] = fabricCanvas;
      _this8.updateViewport(key);
    });
    var name = this.devices.current.name;
    document.addEventListener('keydown', function (_ref40) {
      var key = _ref40.key;
      var draw = _this8.getDraw(name);
      var currentElem = draw.getActiveObject();
      if (currentElem) {
        switch (key) {
          case 'Delete':
            draw.remove(currentElem);
            break;
          default:
        }
      }
    });
  };

  /**
   * Get the canvas wrapper size.
   *
   * @return {{width: number, height: number}} size object
   *
   * @private
   */
  Drawing.prototype.getCanvasWrpSize = function () {
    var _window = window,
      innerHeight = _window.innerHeight,
      innerWidth = _window.innerWidth;
    var _this$canvasWrp = this.canvasWrp,
      clientWidth = _this$canvasWrp.clientWidth,
      parentNode = _this$canvasWrp.parentNode;
    var toolbarWrp = parentNode.querySelector('.toolbar-wrp');
    var height = Math.floor(clientWidth / (innerWidth / innerHeight));
    var maxHeight = Math.floor(innerHeight - toolbarWrp.clientHeight - 2);
    maxHeight = !this.storage.fullscreen ? maxHeight * 0.7 : maxHeight;
    height = height > maxHeight ? maxHeight : height;
    return {
      height: height,
      width: Math.floor(clientWidth)
    };
  };

  /**
   * Get a canvas object.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {HTMLCanvasElement|Object} canvas object
   */
  Drawing.prototype.getCanvas = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    deviceName = deviceName || this.devices.current.name;
    return this.canvas[deviceName] || {};
  };

  /**
   * Get a drawing object.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {Object} drawing object
   */
  Drawing.prototype.getDraw = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    deviceName = deviceName || this.devices.current.name;
    return this.storage.canvas[deviceName] || {};
  };

  /**
   * Update canvas element attributes.
   *
   * @param {string} deviceName canvas device name
   *
   * @private
   */
  Drawing.prototype.updateViewport = function (deviceName) {
    var devices = this.devices;
    var current = devices.current;
    var isActiveClass = 'is-active';
    var canvas = this.getCanvas(deviceName);
    var canvasCurr = this.getCanvas(current.name);
    var canvasCn = canvas.closest('.canvas-item');
    var canvasCnItems = canvasCn.parentNode.children;
    var viewport = this.getCanvasWrpSize();
    var _this$devices$find = this.devices.find(deviceName),
      height = _this$devices$find.height,
      width = _this$devices$find.width;
    var breakpoint = "".concat(viewport.width, "px");
    var scale = _scaleDimensions({
      height: _convertToPixel(height, viewport.height),
      width: _convertToPixel(width, viewport.width),
      maxHeight: viewport.height,
      maxWidth: viewport.width - 2
    });
    height = scale.height;
    width = scale.width;
    if (deviceName !== devices.last().name) {
      breakpoint = devices.next(deviceName).breakpoint;
    }
    Object.entries(canvasCnItems).forEach(function (_ref41) {
      var _ref42 = _slicedToArray(_ref41, 2),
        elem = _ref42[1];
      elem.classList.remove(isActiveClass);
    });
    canvasCurr && canvasCurr.closest('.canvas-item').classList.add(isActiveClass);
    if (deviceName === current.name) {
      this.canvasWrp.style.height = "".concat(height + 1, "px");
    }
    canvasCn.style.height = "".concat(height, "px");
    canvasCn.style.maxWidth = breakpoint;
    this.updateCanvas(deviceName, {
      height: height,
      width: width
    });
  };

  /**
   * Update all viewports.
   */
  Drawing.prototype.updateViewports = function () {
    var _this9 = this;
    this.devices.forEach(function (_ref43) {
      var name = _ref43.name;
      _this9.updateViewport(name);
    });
  };

  /**
   * Update canvas data.
   *
   * @param {string} deviceName device name
   * @param {Object} opts options
   *
   * @private
   */
  Drawing.prototype.updateCanvas = function (deviceName) {
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var draw = this.getDraw(deviceName);
    var canvasCn = this.getCanvas(deviceName).parentNode;
    var height = opts.height,
      width = opts.width;
    canvasCn.style.height = "".concat(height, "px");
    canvasCn.style.width = "".concat(width, "px");
    draw.setHeight(height);
    draw.setWidth(width);
  };

  /**
   * Render canvas.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.render = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    this.getDraw(deviceName).renderAll();
  };

  /**
   * Get background color.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {string} background color
   */
  Drawing.prototype.getBackgroundColor = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return this.getDraw(deviceName).backgroundColor;
  };

  /**
   * Set background color.
   *
   * @param {string} color color
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setBackgroundColor = function (color) {
    var deviceName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    this.getDraw(deviceName).setBackgroundColor(color);
  };

  /**
   * Remove background color.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.removeBackgroundColor = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    this.setBackgroundColor(undefined, deviceName);
  };

  /**
   * Get background image element.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {*} background image element
   */
  Drawing.prototype.getBackgroundImage = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return this.getDraw(deviceName).backgroundImage;
  };

  /**
   * Set background image element.
   *
   * @param {Object|string} src image src
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setBackgroundImage = function (src) {
    var _this10 = this;
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var deviceName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
    var draw = this.getDraw(deviceName);
    fabric.Image.fromURL(src, function (img) {
      var _this10$_scaleImage = _this10._scaleImage(img, opts, draw),
        scaleX = _this10$_scaleImage.scaleX,
        scaleY = _this10$_scaleImage.scaleY;
      var position = _this10._positionImage(img, opts, draw);
      draw.setBackgroundImage(img, draw.renderAll.bind(draw), _objectSpread(_objectSpread(_objectSpread({}, opts), position), {}, {
        scaleX: scaleX,
        scaleY: scaleY
      }));
    });
  };

  /**
   * Update background image element.
   *
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.updateBackgroundImage = function (_ref44) {
    var scale = _ref44.scale,
      originX = _ref44.originX,
      originY = _ref44.originY;
    var deviceName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    var draw = this.getDraw(deviceName);
    var backgroundImage = this.getBackgroundImage(deviceName);
    if (!backgroundImage) {
      return;
    }
    if (backgroundImage.scale !== scale) {
      var _this$_scaleImage = this._scaleImage(backgroundImage, {
          scale: scale,
          originX: originX,
          originY: originY
        }, draw),
        scaleX = _this$_scaleImage.scaleX,
        scaleY = _this$_scaleImage.scaleY;
      backgroundImage.scale = scale;
      backgroundImage.scaleX = scaleX;
      backgroundImage.scaleY = scaleY;
    }
    if (backgroundImage.originX !== originX || backgroundImage.originY !== originY) {
      var position = this._positionImage(backgroundImage, {
        originX: originX,
        originY: originY
      }, draw);
      backgroundImage.top = position.top;
      backgroundImage.left = position.left;
      backgroundImage.originX = position.originX;
      backgroundImage.originY = position.originY;
    }
  };

  /**
   * Update background image element.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.removeBackgroundImage = function () {
    var deviceName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    this.getDraw(deviceName).backgroundImage = undefined;
  };

  /**
   * Set image element.
   *
   * @param {Object|string} src image src
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setImage = function (src) {
    var _this11 = this;
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var deviceName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
    var draw = this.getDraw(deviceName);
    fabric.Image.fromURL(src, function (img) {
      if (img.height > draw.height || img.width > draw.width) {
        var _this11$_scaleImage = _this11._scaleImage(img, {
            scale: 'contain'
          }, draw),
          scaleX = _this11$_scaleImage.scaleX,
          scaleY = _this11$_scaleImage.scaleY;
        img.scaleX = scaleX;
        img.scaleY = scaleY;
      }
      draw.centerObject(img);
      draw.add(img);
    });
  };

  /**
   * Set image element.
   *
   * @param {string} msg text content
   * @param {Object} opts options object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setTextbox = function () {
    var msg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var deviceName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
    var draw = this.getDraw(deviceName);
    var text = new fabric.Textbox(msg, opts);
    draw.centerObject(text);
    draw.add(text);
  };

  /**
   * Scale background image.
   *
   * @param {Object} data data object
   * @param {Object} opts options object
   * @param {Object} draw draw object
   *
   * @return {{scaleX: number, scaleY: number}} background image element
   *
   * @private
   */
  Drawing.prototype._scaleImage = function (_ref45, _ref46, draw) {
    var height = _ref45.height,
      width = _ref45.width;
    var scale = _ref46.scale,
      scaleX = _ref46.scaleX,
      scaleY = _ref46.scaleY;
    var switchVar;
    switch (scale) {
      case 'cover':
        switchVar = _coverDimensions({
          height: height,
          width: width,
          maxHeight: draw.height,
          maxWidth: draw.width
        });
        scaleX = switchVar.ratio;
        scaleY = switchVar.ratio;
        break;
      case 'contain':
        switchVar = _scaleDimensions({
          height: height,
          width: width,
          maxHeight: draw.height,
          maxWidth: draw.width
        });
        scaleX = switchVar.ratio;
        scaleY = switchVar.ratio;
        break;
      default:
        scaleX = draw.width / width;
        scaleY = draw.height / height;
    }
    return {
      scaleX: scaleX,
      scaleY: scaleY
    };
  };

  /**
   * Position background image.
   *
   * @param {Object} data data object
   * @param {Object} opts options object
   * @param {Object} draw draw object
   *
   * @return {{top: number, left: number, originX: string, originY:
   *   string}} postion object
   *
   * @private
   */
  Drawing.prototype._positionImage = function (_ref47, _ref48, draw) {
    var height = _ref47.height,
      width = _ref47.width;
    var originX = _ref48.originX,
      originY = _ref48.originY;
    var top;
    var left;
    switch (originY) {
      case 'top':
        top = 0;
        break;
      case 'bottom':
        top = draw.height;
        break;
      default:
        top = draw.height / 2;
    }
    switch (originX) {
      case 'left':
        left = 0;
        break;
      case 'right':
        left = draw.width;
        break;
      default:
        left = draw.width / 2;
    }
    return {
      top: top,
      left: left,
      originX: originX,
      originY: originY
    };
  };

  /**
   * Banner builder main.
   *
   * @param {HTMLElement} elem The element to attach the editor to
   * @param {string} format The text format for the editor
   *
   * @constructor
   */
  function BannerBuilder(elem, format) {
    /**
     * Storage object.
     *
     * @type {Object}
     */
    this.storage = new Storage(elem, format);

    /**
     * Devices handler.
     *
     * @type {Devices}
     */
    this.devices = new Devices(this.storage.settings.devices);

    /**
     * Markup handler.
     *
     * @type {Markup}
     */
    this.markup = new Markup(this);

    /**
     * Markup handler.
     *
     * @type {Drawing}
     */
    this.drawing = new Drawing(this);

    /**
     * Toolbar handler.
     *
     * @type {Toolbar}
     */
    this.toolbar = new Toolbar(this);

    // build markup
    var _this$storage = this.storage,
      element = _this$storage.element,
      container = _this$storage.container;
    container.classList.add('banner-builder');
    container.appendChild(this.toolbar.build());
    container.appendChild(this.drawing.build());
    element.parentNode.appendChild(container);

    // run drawing handler
    this.drawing.run();
  }

  /**
   * Detach banner builder event.
   *
   * @param {string} trigger The event trigger for the detach
   */
  BannerBuilder.prototype.detach = function (trigger) {
    var _this$storage2 = this.storage,
      element = _this$storage2.element,
      container = _this$storage2.container;
    if (trigger === 'serialize') {
      this.storage.save();
    } else {
      element.value = '';
      element.dataset.editorValueOriginal = '';
      element.style.display = 'block';
      container.style.display = 'none';
    }
  };

  /**
   * On change banner builder event.
   */
  BannerBuilder.prototype.onChange = function () {
    var _this$storage3 = this.storage,
      element = _this$storage3.element,
      container = _this$storage3.container;
    element.style.display = 'none';
    container.style.display = 'block';
  };

  /**
   * Banner builder default settings.
   */
  BannerBuilder.defSettings = {
    devices: {
      mobile: {
        breakpoint: '0',
        height: '100vh',
        width: '360px'
      },
      tablet: {
        breakpoint: '768px',
        height: '100vh',
        width: '1024px'
      },
      desktop: {
        breakpoint: '1024px',
        height: '100vh',
        width: '1920px'
      }
    }
  };

  /**
   * Banner builder settings form layout
   */
  BannerBuilder.settingsForm = {
    devices: {
      _type: 'tabs'
    }
  };

  /**
   * Drupal editor object.
   *
   * @type {*}
   */
  editors.banner_builder = {
    attach: function attach(element, format) {
      if (!element.hasOwnProperty('BannerBuilder')) {
        element.BannerBuilder = new BannerBuilder(element, format);
      }
    },
    detach: function detach(element, format, trigger) {
      element.hasOwnProperty('BannerBuilder') && element.BannerBuilder.detach(trigger);
    },
    onChange: function onChange(element) {
      element.hasOwnProperty('BannerBuilder') && element.BannerBuilder.onChange();
    }
  };

  /**
   * Return a list off all available fonts and load custom fonts.
   *
   * @param {Object} format format settings
   *
   * @return {Array} available fonts
   */
  function _availableFonts(format) {
    var _document = document,
      fonts = _document.fonts;
    var editorSettings = format.editorSettings;
    var _ref49 = editorSettings || {},
      featuresSettings = _ref49.featuresSettings;
    var _ref50 = featuresSettings.fonts || {},
      defaultFonts = _ref50.defaultFonts,
      customFonts = _ref50.customFonts;
    var fontAvailable = [];
    // noinspection SpellCheckingInspection
    var fontCheck = new Set([
    // Windows 10
    'Arial', 'Arial Black', 'Bahnschrift', 'Calibri', 'Cambria', 'Cambria Math', 'Candara', 'Comic Sans MS', 'Consolas', 'Constantia', 'Corbel', 'Courier New', 'Ebrima', 'Franklin Gothic Medium', 'Gabriola', 'Gadugi', 'Georgia', 'HoloLens MDL2 Assets', 'Impact', 'Ink Free', 'Javanese Text', 'Leelawadee UI', 'Lucida Console', 'Lucida Sans Unicode', 'Malgun Gothic', 'Marlett', 'Microsoft Himalaya', 'Microsoft JhengHei', 'Microsoft New Tai Lue', 'Microsoft PhagsPa', 'Microsoft Sans Serif', 'Microsoft Tai Le', 'Microsoft YaHei', 'Microsoft Yi Baiti', 'MingLiU-ExtB', 'Mongolian Baiti', 'MS Gothic', 'MV Boli', 'Myanmar Text', 'Nirmala UI', 'Palatino Linotype', 'Segoe MDL2 Assets', 'Segoe Print', 'Segoe Script', 'Segoe UI', 'Segoe UI Historic', 'Segoe UI Emoji', 'Segoe UI Symbol', 'SimSun', 'Sitka', 'Sylfaen', 'Symbol', 'Tahoma', 'Times New Roman', 'Trebuchet MS', 'Verdana', 'Webdings', 'Wingdings', 'Yu Gothic',
    // macOS
    'American Typewriter', 'Andale Mono', 'Arial', 'Arial Black', 'Arial Narrow', 'Arial Rounded MT Bold', 'Arial Unicode MS', 'Avenir', 'Avenir Next', 'Avenir Next Condensed', 'Baskerville', 'Big Caslon', 'Bodoni 72', 'Bodoni 72 Oldstyle', 'Bodoni 72 Smallcaps', 'Bradley Hand', 'Brush Script MT', 'Chalkboard', 'Chalkboard SE', 'Chalkduster', 'Charter', 'Cochin', 'Comic Sans MS', 'Copperplate', 'Courier', 'Courier New', 'Didot', 'DIN Alternate', 'DIN Condensed', 'Futura', 'Geneva', 'Georgia', 'Gill Sans', 'Helvetica', 'Helvetica Neue', 'Herculanum', 'Hoefler Text', 'Impact', 'Lucida Grande', 'Luminari', 'Marker Felt', 'Menlo', 'Microsoft Sans Serif', 'Monaco', 'Noteworthy', 'Optima', 'Palatino', 'Papyrus', 'Phosphate', 'Rockwell', 'Savoye LET', 'SignPainter', 'Skia', 'Snell Roundhand', 'Tahoma', 'Times', 'Times New Roman', 'Trattatello', 'Trebuchet MS', 'Verdana', 'Zapfino']);
    if (defaultFonts) {
      fonts.ready.then(function () {
        fontCheck.forEach(function (font) {
          if (fonts.check("12px \"".concat(font, "\""))) {
            fontAvailable.push(font);
          }
        });
      });
    }
    if (customFonts) {
      fontAvailable.push.apply(fontAvailable, _toConsumableArray(customFonts.split(/\r?\n/) || []));
    }
    return fontAvailable.sort();
  }

  /**
   * Scale dimensions.
   *
   * @param {{width: number, height: number, maxHeight: number, maxWidth:
   *   number}} obj data object
   *
   * @return {{width: number, height: number, ratio: number}} scaled data
   *   object
   */
  function _scaleDimensions(_ref51) {
    var height = _ref51.height,
      width = _ref51.width,
      maxHeight = _ref51.maxHeight,
      maxWidth = _ref51.maxWidth;
    var ratio = Math.min(maxWidth / width, maxHeight / height);
    return {
      height: Math.floor(ratio * height),
      width: Math.floor(ratio * width),
      ratio: ratio
    };
  }

  /**
   * Crop dimensions.
   *
   * @param {{width: number, height: number, maxHeight: number, maxWidth:
   *   number}} obj data object
   *
   * @return {{width: number, height: number, ratio: number}} scaled data
   *   object
   */
  function _coverDimensions(_ref52) {
    var height = _ref52.height,
      width = _ref52.width,
      maxHeight = _ref52.maxHeight,
      maxWidth = _ref52.maxWidth;
    var ratio = Math.max(maxWidth / width, maxHeight / height);
    return {
      height: Math.floor(ratio * height),
      width: Math.floor(ratio * width),
      ratio: ratio
    };
  }

  /**
   * Connect css unit string to number.
   *
   * @param {string|number} val css unit string
   * @param {number} base calc base
   *
   * @return {number} pixel value
   */
  function _convertToPixel(val) {
    var base = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    if (typeof val === 'number') {
      return val;
    }
    var _splitCssUnit2 = _splitCssUnit(val),
      number = _splitCssUnit2.number,
      unit = _splitCssUnit2.unit;
    if (number) {
      switch (unit) {
        case '%':
        case 'vh':
        case 'vw':
          return base * (number / 100);
        default:
          return number;
      }
    }
    return 0;
  }

  /**
   * Split css unit into number and unit
   *
   * @param {string} str css unit string
   *
   * @return {{number: (number), unit: (string)}} object
   */
  function _splitCssUnit(str) {
    var regex = /(\d*)(.*)/gm;
    var m = regex.exec(str);
    var _m = _slicedToArray(m, 3),
      number = _m[1],
      unit = _m[2];
    return {
      number: parseInt(number || '0', 10),
      unit: unit || ''
    };
  }

  /**
   * Merge two objects, second override first.
   *
   * @param {Object} objA first object
   * @param {Object} objB second object
   *
   * @return {Object} merged object
   */
  function _mergeObj(objA, objB) {
    Object.entries(objB).forEach(function (elem) {
      var _elem = _slicedToArray(elem, 1),
        p = _elem[0];
      try {
        if (_typeof(objB[p]) === 'object') {
          objA[p] = _mergeObj(objA[p], objB[p]);
        } else {
          objA[p] = objB[p];
        }
      } catch (e) {
        objA[p] = objB[p];
      }
    });
    return objA;
  }

  /**
   * Clone an object and create a new reference.
   *
   * @param {Object} obj object to clone
   *
   * @return {Object} cloned object
   */
  function _cloneObj(obj) {
    return JSON.parse(JSON.stringify(obj || {})) || {};
  }

  /**
   * Remove property ky key recursively.
   *
   * @param {Object} obj object to clean
   */
  function _removePrivateProp(obj) {
    if (_typeof(obj) === 'object') {
      Object.entries(obj).forEach(function (_ref53) {
        var _ref54 = _slicedToArray(_ref53, 1),
          prop = _ref54[0];
        if (prop[0] === '_') {
          delete obj[prop];
        } else if (_typeof(obj[prop]) === 'object') {
          _removePrivateProp(obj[prop]);
        }
      });
    }
  }

  /**
   * Remove property ky key recursively and return a new object.
   *
   * @param {Object} obj object to clean.
   *
   * @return {Object} clean object
   */
  function _removePrivatePropNew(obj) {
    if (_typeof(obj) !== 'object') {
      return {};
    }
    var ret = _cloneObj(obj);
    _removePrivateProp(ret);
    return ret;
  }

  /**
   * Build form name.
   *
   * @param {array} arr String to format
   *
   * @returns {string} form name string
   */
  function _buildFormName() {
    var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var _Object$keys = Object.keys(arr),
      length = _Object$keys.length;
    if (length) {
      arr = _cloneObj(arr);
      var _arr$splice = arr.splice(0, 1),
        _arr$splice2 = _slicedToArray(_arr$splice, 1),
        key = _arr$splice2[0];
      if (length === 1) {
        return key || '';
      }
      return "".concat(key || '', "[").concat(arr.join(']['), "]");
    }
    return '';
  }

  /**
   * Parse form name.
   *
   * @param {string} str form name string
   *
   * @returns {array} name array
   */
  function _parseFormName(str) {
    var ret = [];
    var regex = /^([\w\d\-_]*)\[(.*)]/gm;
    var m = regex.exec(str) || [];
    var _m$splice = m.splice(1),
      _m$splice2 = _slicedToArray(_m$splice, 2),
      first = _m$splice2[0],
      second = _m$splice2[1];
    ret.push(first || str);
    if (typeof second === 'string') {
      ret.push.apply(ret, _toConsumableArray(second.split('][')));
    }
    return ret;
  }

  /**
   * Set first letter to uppercase.
   * @param {string} str string
   *
   * @return {string} string
   */
  function _strFirstLetterUc(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * Return random string.
   *
   * @returns {string} random string
   */
  function _randStr() {
    return (Math.random() + 1).toString(36).substring(2);
  }
})(jQuery, Drupal, fabric);
