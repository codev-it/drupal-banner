// noinspection All

/**
 * @file
 * Behaviors of codev_banner module.
 */
(function ($, Drupal, once) {
  var behaviors = Drupal.behaviors;
  behaviors.codevBannerSlick = {
    attach: function attach(context) {
      once('init-slick-slider', '[data-slick]', context).forEach(function (item) {
        var $item = $(item);
        if ($item.children().length) {
          var options = $item.data('slick') || {};
          // noinspection JSUnresolvedFunction
          $item.slick(options);
        }
      });
    }
  };
})(jQuery, Drupal, once);
