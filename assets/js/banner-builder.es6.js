// noinspection DuplicatedCode

(($, Drupal, fabric) => {
  const { editors, dialog, t } = Drupal;

  /**
   * Banner builder storage handler.
   *
   * @param {HTMLTextAreaElement} element persist storage element
   * @param {Object} format editor format settings
   *
   * @constructor
   */
  function Storage(element, format) {
    /**
     * Element object.
     *
     * @type {HTMLTextAreaElement}
     */
    this.element = element;

    /**
     * Formate object.
     *
     * @type {Object}
     */
    this.formate = format;

    /**
     * Settings object.
     *
     * @type {HTMLDivElement}
     */
    this.settings = _cloneObj(BannerBuilder.defSettings);

    /**
     * Container object.
     *
     * @type {HTMLDivElement}
     */
    this.container = document.createElement('div');

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvas = {};

    /**
     * List off all available fonts.
     *
     * @type {Array}
     */
    this.fonts = _availableFonts(format);

    /**
     * Fullscreen status.
     *
     * @type {boolean}
     */
    this.fullscreen = false;

    // merge format editor settings
    const { editorSettings } = format;
    const { devices } = editorSettings;
    typeof devices === 'object' && _mergeObj(this.settings.devices, devices);

    // load from persist element storage
    this.load();
  }

  /**
   * Load persist data from element and update storage.
   */
  Storage.prototype.load = function() {
    const { value } = this.element;
    if (value && value.charAt(0) === '{') {
      const { settings, canvas } = JSON.parse(value);
      settings && _mergeObj(this.settings, settings);
      Object.entries(canvas || {}).forEach(([key]) => {
        if (!this.canvas[key] && canvas[key]) {
          this.canvas[key] = canvas[key];
        }
      });
    }
  };

  /**
   * Save the current settings and infos to element.
   */
  Storage.prototype.save = function() {
    const svg = {};
    Object.entries(this.canvas).forEach(([key, canvas]) => {
      svg[key] = canvas.toSVG();
    });
    this.element.setAttribute('data-editor-value-is-changed', 'true');
    this.element.value = JSON.stringify({
      settings: _removePrivatePropNew(this.settings),
      canvas  : this.canvas,
      svg
    });
  };

  /**
   * Get settings by keys.
   *
   * @param {string|array} key settings key
   * @param {*} fallback fallback
   *
   * @return {*} Settings object
   */
  Storage.prototype.getSetting = function(key, fallback = undefined) {
    let scope = this.settings;
    switch (typeof key) {
      case 'string':
        return scope[key] || fallback;
      case 'object':
        if (!key.length) {
          return fallback;
        }
        key.forEach((val) => {
          if (typeof scope === 'object' && scope[val] !== undefined) {
            scope = scope[val];
          } else {
            scope = fallback;
          }
        });
        return scope;
      default:
        return fallback;
    }
  };

  /**
   * Set settings by keys.
   *
   * @param {string|array} key settings key
   * @param {*} val settings values
   */
  Storage.prototype.setSetting = function(key, val) {
    let scope = this.settings;
    const max = key.length;
    switch (typeof key) {
      case 'string':
        scope[key] = val;
        break;
      case 'object':
        if (max) {
          key.forEach((k, index) => {
            if (scope[k] !== undefined) {
              if (index === max - 1) {
                scope[k] = val;
              } else {
                scope = scope[k];
              }
            } else {
              scope[k] = index === max - 1 ? val : {};
              scope = scope[k];
            }
          });
        }
        break;
      default:
    }
  };

  /**
   * Banner builder device handler.
   *
   * @param {Object} devices default devices settings
   *
   * @constructor
   */
  function Devices(devices) {
    /**
     * Devices list object.
     *
     * @type {Object}
     */
    this.list = devices;

    /**
     * Devices list entities.
     *
     * @type {[string, unknown][]}
     */
    this.entries = Object.entries(this.list);

    // eslint-disable-next-line prefer-destructuring
    /**
     * Current device object.
     *
     * @type {Object}
     */
    this.current = this.last();
  }

  /**
   * Iterate device entities.
   *
   * @param {function} fn for each element function
   */
  Devices.prototype.forEach = function(fn) {
    if (typeof fn === 'function') {
      this.entries.forEach(([name, data], index) => {
        fn({ name, ...(data || {}) }, index);
      });
    }
  };

  /**
   * Find device data by name.
   *
   * @param {string} deviceName device name
   *
   * @return {Object|false} device
   *   object
   */
  Devices.prototype.find = function(deviceName) {
    if (this.list[deviceName] !== undefined) {
      return { name: deviceName, ...this.list[deviceName] };
    }
    return false;
  };

  /**
   * Get first device element.
   *
   * @return {Object} device object
   */
  Devices.prototype.first = function() {
    const [name, data] = this.entries.at(0);
    return { name, ...data };
  };

  /**
   * Get last device element.
   *
   * @return {Object} device object
   */
  Devices.prototype.last = function() {
    const [name, data] = this.entries.at(-1);
    return { name, ...data };
  };

  /**
   * Get next device element.
   *
   * @param {string} deviceName current device
   *
   * @return {Object} device object
   */
  Devices.prototype.next = function(deviceName = '') {
    return this._sibling(deviceName);
  };

  /**
   * Get preview device element.
   *
   * @param {string} deviceName current device
   *
   * @return {Object} device object
   */
  Devices.prototype.prev = function(deviceName = '') {
    return this._sibling(deviceName, 'prev');
  };

  /**
   * Get device sibling by current device name.
   *
   * @param {string} deviceName current device
   * @param {string} direct direction
   *
   * @return {Object} device object
   *
   * @private
   */
  Devices.prototype._sibling = function(deviceName = '', direct = 'next') {
    const maxIndex = this.entries.length - 1;
    const current = this.find(deviceName) || this.current;
    let sibling = current;
    this.forEach(({ name }, i) => {
      const newIndex = direct === 'next' ? i + 1 : i - 1;
      if (name === current.name && newIndex >= 0 && newIndex <= maxIndex) {
        const [key, val] = this.entries.at(newIndex);
        sibling = { key, ...(val || {}) };
      }
    });
    return sibling;
  };

  /**
   * Change current device.
   *
   * @param {string} deviceName current device
   *
   * @return {boolean} device object
   */
  Devices.prototype.change = function(deviceName = '') {
    if (this.current.name !== deviceName) {
      const newDevice = this.find(deviceName);
      if (newDevice) {
        this.current = newDevice;
        return true;
      }
    }
    return false;
  };

  /**
   * Banner builder ui markup handler.
   *
   * @param {BannerBuilder} self banner builder object
   *
   * @constructor
   */
  function Markup(self) {
    const { storage, devices } = self;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;
  }

  /**
   * Create button element.
   *
   * @param {{name: string, value: string, icon: Array, classes: Array,
   *   data: Object}} object button name
   *
   * @return {HTMLSpanElement} button element
   */
  Markup.prototype.button = function({ title, value, icon, classes, data }) {
    const elem = document.createElement('span');

    elem.textContent = value || '';
    elem.classList.add(...(classes || []));

    if (title) {
      title = t(title);
      elem.setAttribute('title', title);
      elem.setAttribute('alt', title);
    }
    if (Object.keys(icon || []).length) {
      const iconElem = document.createElement('i');
      iconElem.classList.add(...icon);
      elem.appendChild(iconElem);
    }

    Object.entries(data || {}).forEach(([key, val]) => {
      elem.dataset[key] = val.toString();
    });

    return elem;
  };

  /**
   * Create button group element.
   *
   * @param {array} arr buttons
   *
   * @return {HTMLDivElement} buttons element
   */
  Markup.prototype.buttons = function(arr) {
    const elem = document.createElement('div');
    elem.classList.add('button-group');
    arr.forEach((data) => {
      elem.appendChild(this.button(data));
    });
    return elem;
  };

  /**
   * Create a tabs element.
   *
   * @param {Object} data data object
   *
   * @return {{tabsContent: {}, tabs: HTMLDivElement}} tabs object
   */
  Markup.prototype.tabs = function(data) {
    const tabs = document.createElement('div');
    const header = document.createElement('ul');
    const content = document.createElement('div');
    const tabsContent = {};

    tabs.classList.add('tabs-cn');
    header.classList.add('tabs');
    content.classList.add('tabs-content');

    let index = 0;
    Object.entries(data).forEach(([key, item]) => {
      if (key[0] !== '_') {
        const { _button } = item;
        let { _isActive } = item;
        const isActiveClass = 'is-active';
        const itemId = `tab-content-${key}-${_randStr()}`;
        const headerItem = document.createElement('li');
        const contentItem = document.createElement('div');
        _isActive = _isActive || index === 0;

        headerItem.textContent = _button ? t(_button) : key;
        headerItem.classList.add('tab-title');
        headerItem.addEventListener('click', ({ target }) => {
          if (!target.classList.contains(isActiveClass)) {
            const activeTitle = target.parentNode.querySelector('.is-active');
            target.classList.add(isActiveClass);
            activeTitle && activeTitle.classList.remove(isActiveClass);
            Object.entries(content.children).forEach(([, entry]) => {
              if (entry.id === itemId) {
                entry.classList.add(isActiveClass);
              } else {
                entry.classList.remove(isActiveClass);
              }
            });
          }
        });

        contentItem.id = itemId;
        contentItem.classList.add('tab-panel');

        if (_isActive) {
          headerItem.classList.add(isActiveClass);
          contentItem.classList.add(isActiveClass);
        }

        tabsContent[key] = contentItem;

        header.appendChild(headerItem);
        content.appendChild(contentItem);
        index += 1;
      }
    });

    tabs.appendChild(header);
    tabs.appendChild(content);

    return { tabs, tabsContent };
  };

  /**
   * Create form input field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLInputElement} input field markup
   */
  Markup.prototype.formInputField = function({
    _type,
    _name,
    _value,
    _defaultValue,
    _fieldType,
    _accept,
    _required
  }) {
    _name = _name || '';
    _value = _value || '';
    _defaultValue = _defaultValue || '';
    _fieldType = _fieldType || 'input';
    const elem = document.createElement(_fieldType);
    const valueKeys = _value.split('.') || [];
    const fieldId = valueKeys.join('-');
    const fieldName = _buildFormName(valueKeys) || _name;
    const settingVal = this.storage.getSetting(valueKeys);
    let fieldVal = settingVal !== undefined ? settingVal : _defaultValue;
    fieldVal = fieldVal !== undefined ? fieldVal : _value;
    elem.id = fieldId;
    elem.setAttribute('name', fieldName);
    elem.type = _type || 'text';
    elem.value = fieldVal;
    elem.classList.add(`field-${_fieldType}`);
    _required && elem.setAttribute('required', 'true');
    _accept && elem.setAttribute('accept', _accept.join(', '));
    return elem;
  };

  /**
   * Create form select field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLSelectElement} select field markup
   */
  Markup.prototype.formSelectField = function({
    _type,
    _name,
    _value,
    _defaultValue,
    _options,
    _required
  }) {
    _name = _name || '';
    _value = _value || '';
    _options = _options || {};
    _defaultValue = _defaultValue || '';
    const valueKeys = _value.split('.') || [];
    const fieldId = valueKeys.join('-');
    const fieldName = _buildFormName(valueKeys) || _name;
    const settingVal = this.storage.getSetting(valueKeys);
    let fieldVal = settingVal !== undefined ? settingVal : _defaultValue;
    fieldVal = fieldVal !== undefined ? fieldVal : _value;
    const isOptionArray = Array.isArray(_options);
    const elem = document.createElement('select');
    elem.id = fieldId;
    elem.setAttribute('name', fieldName);
    elem.classList.add(`field-select`);
    _required && elem.setAttribute('required', 'true');

    Object.entries(_options).forEach(([key, val]) => {
      const option = document.createElement('option');
      let value = isOptionArray ? val : key;
      if (typeof value === 'object') {
        value = value.textContent;
      }
      option.value = value.toString();
      if (value.trim() === fieldVal.trim()) {
        option.setAttribute('selected', 'true');
      }
      if (val instanceof HTMLElement) {
        option.appendChild(val);
      } else if (val) {
        option.textContent = val.toString();
      }
      elem.appendChild(option);
    });

    return elem;
  };

  /**
   * Create form field.
   *
   * @param {Object} data options object
   *
   * @return {HTMLDivElement} field markup
   */
  Markup.prototype.formField = function({
    _type,
    _name,
    _label,
    _value,
    _defaultValue,
    _description,
    _fieldType,
    _options,
    _classes,
    _accept,
    _required
  }) {
    _name = _name || '';
    _value = _value || '';
    _fieldType = _fieldType || 'input';
    const elem = document.createElement('div');
    const label = _label !== undefined && _label !== '' ? t(_label) : '';
    const valueKeys = _value.split('.') || [];
    const fieldId = valueKeys.join('-');
    const labelElem = document.createElement('label');

    elem.classList.add('field', `field-type-${_type}`, ...(_classes || []));
    labelElem.textContent = label;
    labelElem.setAttribute('for', fieldId);
    labelElem.classList.add('field-label');
    _required && labelElem.classList.add('form-required');

    elem.appendChild(labelElem);

    switch (_type) {
      case 'select':
        elem.appendChild(
          this.formSelectField({
            _type,
            _name,
            _value,
            _defaultValue,
            _options,
            _required
          })
        );
        break;
      default:
        elem.appendChild(
          this.formInputField({
            _type,
            _name,
            _value,
            _defaultValue,
            _fieldType,
            _accept,
            _required
          })
        );
    }

    if (_description) {
      const descriptionElem = document.createElement('div');
      descriptionElem.textContent = _description;
      descriptionElem.classList.add('field-description');
      elem.appendChild(descriptionElem);
    }

    return elem;
  };

  /**
   * Build recursively elements.
   *
   * @param {Object} data data object
   *
   * @return {HTMLDivElement} markup
   */
  Markup.prototype.buildFormElements = function(data) {
    const { _type, _label, _tag, _classes } = data;
    let elem = document.createElement('div');
    const belowItems = Object.entries(_removePrivatePropNew(data));
    let belowElemRef = {};

    elem.classList.add('field-container');

    let switchVar;
    switch (_type) {
      case 'tabs':
        switchVar = this.tabs(data);
        belowElemRef = switchVar.tabsContent;
        elem = switchVar.tabs;
        break;
      case 'html_tag':
        elem = document.createElement(_tag || 'p');
        elem.textContent = _label || '';
        elem.classList.add(...(_classes || []));
        break;
      case 'textfield':
        elem = this.formField({
          ...data,
          _type: 'text'
        });
        break;
      case 'select':
        elem = this.formField({
          ...data,
          _type: 'select'
        });
        break;
      case 'color':
        elem = this.formField({
          ...data,
          _type: 'color'
        });
        break;
      case 'file':
        elem = this.formField({
          ...data,
          _type: 'file'
        });
        break;
      default:
        if (_label) {
          switchVar = document.createElement('span');
          switchVar.textContent = t(_label);
          switchVar.classList.add('title', ...(_classes || []));
          elem.appendChild(switchVar);
        }
    }

    if (belowItems.length) {
      belowItems.forEach(([key]) => {
        const belowElem = belowElemRef[key] || elem;
        belowElem.appendChild(this.buildFormElements(data[key]));
      });
    }

    return elem;
  };

  /**
   * Create form elements.
   *
   * @param {string} id form id
   * @param {Object} data data object
   *
   * @return {{}} form
   *   element
   */
  Markup.prototype.form = function(id, data = {}) {
    const error = {};
    const form = document.createElement('form');
    form.id = id;
    form.classList.add('banner-builder-form');
    form.appendChild(this.buildFormElements(data || {}));

    return {
      form,

      /**
       * Return form data object.
       *
       * @return {FormData} form data
       */
      formData() {
        return new FormData(form);
      },

      /**
       * Validate form data.
       *
       * @return {boolean} validation status
       */
      formValide() {
        let valide = true;
        const formData = this.formData();
        const getFieldsToValidate = (elem, arr = []) => {
          const below = _removePrivatePropNew(elem);
          if (elem._required || elem._accept || elem._validate) {
            arr.push(elem);
          }
          if (below) {
            Object.entries(below).forEach(([key]) => {
              getFieldsToValidate(elem[key], arr);
            });
          }
          return arr;
        };

        Object.entries(getFieldsToValidate(data)).forEach(
          ([, { _type, _name, _value, _required, _accept, _validate }]) => {
            let fieldName = _name;
            if (_value) {
              fieldName = _buildFormName(_value.split('.') || []);
            }
            const value = formData.get(fieldName);

            if (_required) {
              let showError = false;
              switch (_type) {
                case 'file':
                  if (!value.size) {
                    showError = true;
                  }
                  break;
                default:
                  if (!value) {
                    showError = true;
                  }
              }
              if (showError) {
                this.setFormError(
                  fieldName,
                  'required',
                  t('Field is required.')
                );
                valide = false;
              } else {
                this.removeFormError(fieldName, 'required');
              }
            }

            if (_accept) {
              if (value.size && !_accept.includes(value.type)) {
                this.setFormError(
                  fieldName,
                  'accept',
                  t('File has wrong MIME type.')
                );
                valide = false;
              } else {
                this.removeFormError(fieldName, 'accept');
              }
            }
          }
        );

        return valide;
      },

      /**
       * Set form error.
       *
       * @param {string} name field name
       * @param {string} type type error
       * @param {string} msg error message
       */
      setFormError(name, type = '', msg = '') {
        const key = type !== '' ? `${name}-${type}` : name;
        const field = form.querySelector(`[name="${name}"]`);
        if (field && !error[key]) {
          const wrp = field.parentNode;
          const elem = document.createElement('div');
          elem.classList.add('form-item--error-message');
          elem.textContent = msg;
          wrp.appendChild(elem);
          error[key] = elem;
        }
      },

      /**
       * Remove form error.
       *
       * @param {string} name field name
       * @param {string} type type error
       */
      removeFormError(name, type = '') {
        const key = type !== '' ? `${name}-${type}` : name;
        if (error[key]) {
          error[key].remove();
          delete error[key];
        }
      }
    };
  };

  /**
   * Polyfill HTML5 dialog element with jQueryUI.
   *
   * @param {HTMLElement} element
   *   The element that holds the dialog.
   * @param {Object} options
   *   jQuery UI options to be passed to the dialog.
   *
   * @return {Drupal.dialog~dialogDefinition}
   *   The dialog instances.
   */
  Markup.prototype.dialog = function(element, options = {}) {
    const wrp = document.createElement('div');
    wrp.appendChild(element);
    return dialog(wrp, options);
  };

  /**
   * Banner builder toolbar handler.
   *
   * @param {BannerBuilder} self banner builder object
   *
   * @constructor
   */
  function Toolbar(self) {
    const { storage, devices, markup, drawing } = self;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;

    /**
     * Markup handler.
     *
     * @type {BannerBuilder.markup|Markup}
     */
    this.markup = markup;

    /**
     * Markup drawing.
     *
     * @type {BannerBuilder.drawing|Drawing}
     */
    this.drawing = drawing;

    /**
     * Toolbar container element.
     *
     * @type {HTMLDivElement}
     */
    this.toolbar = document.createElement('div');

    /**
     * Toolbar middle element.
     *
     * @type {HTMLDivElement}
     */
    this.toolbarMiddle = document.createElement('div');
  }

  /**
   * Toolbar elements data.
   *
   * @return {Object} toolbar data object
   */
  Toolbar.prototype.toolbarElements = function() {
    const toolbar = Toolbar.toolbarElements;
    const { editorSettings } = this.storage.formate;
    const { features } = editorSettings;

    /**
     * Remove disabled elements.
     *
     * @param {Object} elements elements object to formate
     * @param {Object} data element status data object
     */
    const removeDisabledElements = (elements, data) => {
      Object.entries(data).forEach(([key, enable]) => {
        if (!enable && elements[key] !== undefined) {
          delete elements[key];
        }
      });
    };

    removeDisabledElements(toolbar.left, features);
    removeDisabledElements(toolbar.right, features);
    return toolbar;
  };

  /**
   * Render toolbar elements.
   *
   * @param {Object} data toolbar data
   *
   * @return {HTMLDivElement} toolbar element
   */
  Toolbar.prototype.buildElements = function(data) {
    const { devices, markup } = this;
    const toolbar = document.createElement('div');
    const currentDevice = devices.current;

    Object.entries(data).forEach(([key, value]) => {
      let elem;
      let switchVar;
      const { type, title, icon, options, defaultValue, click, change } = value;
      switch (type) {
        case 'devicesChanger':
          switchVar = [];
          devices.forEach((device) => {
            switchVar.push({
              value  : device.name,
              classes:
                currentDevice.name === device.name
                  ? ['toolbar-button', 'is-active']
                  : ['toolbar-button'],
              data   : {
                device: device.name
              }
            });
          });
          elem = markup.buttons(switchVar);
          elem.classList.add('device-changer');
          break;
        case 'select':
          elem = markup.formField({
            _type        : 'select',
            _label       : title,
            _name        : key,
            _options     : options,
            _defaultValue: defaultValue
          });
          break;
        case 'spacer':
          elem = document.createElement('span');
          elem.classList.add('toolbar-spacer');
          break;
        default:
          elem = markup.button({
            title,
            icon,
            classes: ['toolbar-button']
          });
      }

      if (click) {
        elem.addEventListener('click', click.bind(this));
      }

      if (change) {
        elem.addEventListener('change', change.bind(this));
      }

      elem && toolbar.appendChild(elem);
    });

    return toolbar;
  };

  /**
   * Build the toolbar markup.
   *
   * @returns {HTMLDivElement} toolbar element
   */
  Toolbar.prototype.build = function() {
    const { left, right } = this.toolbarElements();
    const toolbarLeft = this.buildElements(left);
    const toolbarRight = this.buildElements(right);

    this.toolbar.classList.add('toolbar-wrp', 'row');
    toolbarLeft.classList.add('left', 'column');
    this.toolbarMiddle.classList.add('middle', 'column');
    toolbarRight.classList.add('right', 'column');

    this.toolbar.appendChild(toolbarLeft);
    this.toolbar.appendChild(this.toolbarMiddle);
    this.toolbar.appendChild(toolbarRight);

    return this.toolbar;
  };

  /**
   * Toolbar elements data.
   *
   * @type {Object}
   */
  Toolbar.toolbarElements = {
    left : {
      backgroundImage: {
        title: 'Background image',
        icon : ['fas', 'fa-images'],
        align: 'left',
        buildFormElements(name) {
          return {
            color   : {
              _type        : 'color',
              _name        : 'color',
              _label       : 'Background color',
              _description : 'Select background color',
              _value       : `backgroundImage.${name}.color`,
              _defaultValue: '#ffffff'
            },
            file    : {
              _type       : 'file',
              _name       : 'file',
              _label      : 'Background image',
              _description: 'Select image to upload. Accept: PNG, JPG, GIF',
              _accept     : ['image/png', 'image/jpeg', 'image/gif']
            },
            scale   : {
              _type       : 'select',
              _name       : 'scale',
              _label      : 'Background image select',
              _description: 'Select background image scale',
              _value      : `backgroundImage.${name}.scale`,
              _options    : ['cover', 'contain']
            },
            position: {
              _type        : 'select',
              _name        : 'scale',
              _label       : 'Background image position',
              _description : 'Select background image position',
              _value       : `backgroundImage.${name}.position`,
              _defaultValue: 'center-center',
              _options     : {
                'top-left'     : t('Top left'),
                'top-center'   : t('Top center'),
                'top-right'    : t('Top right'),
                'center-left'  : t('Center left'),
                'center-center': t('Center center'),
                'center-right' : t('Center right'),
                'bottom-left'  : t('Bottom left'),
                'bottom-center': t('Bottom center'),
                'bottom-right' : t('Bottom right')
              }
            }
          };
        },
        click() {
          const { storage, devices, markup, drawing } = this;
          const currentDevice = devices.current;
          const scopeFn = Toolbar.toolbarElements.left.backgroundImage;
          const formElements = scopeFn.buildFormElements(currentDevice.name);
          const formMarkup = markup.form(
            'background-image-upload-form',
            formElements
          );
          const { form, formData } = formMarkup;
          const colorFieldNameArr = [
            'backgroundImage',
            currentDevice.name,
            'color'
          ];
          const colorFieldName = _buildFormName(colorFieldNameArr);
          const scaleFieldName = _buildFormName([
            'backgroundImage',
            currentDevice.name,
            'scale'
          ]);
          const positionFieldName = _buildFormName([
            'backgroundImage',
            currentDevice.name,
            'position'
          ]);
          const backgroundImageUploadDialog = markup.dialog(form, {
            title  : t('Background image'),
            buttons: [
              {
                text : t('Save'),
                class: 'button button--primary',
                click() {
                  if (!formMarkup.formValide()) {
                    return;
                  }
                  const reader = new FileReader();
                  const formDataValues = formData();
                  const scale = formDataValues.get(scaleFieldName);
                  const position = formDataValues.get(positionFieldName);
                  const [originY, originX] = position.split('-');
                  formDataValues.forEach((value, name) => {
                    const formName = _parseFormName(name);
                    switch (name) {
                      case colorFieldName:
                        drawing.setBackgroundColor(value);
                        storage.setSetting(formName, value);
                        break;
                      case 'file':
                        if (value.size) {
                          reader.onload = ({ target }) => {
                            drawing.setBackgroundImage(target.result, {
                              scale,
                              originX,
                              originY
                            });
                          };
                          reader.readAsDataURL(value);
                        }
                        break;
                      case scaleFieldName:
                      case positionFieldName:
                        drawing.updateBackgroundImage({
                          scale,
                          originX,
                          originY
                        });
                        storage.setSetting(formName, value);
                        break;
                      default:
                        storage.setSetting(formName, value);
                    }
                  });

                  drawing.render();
                  backgroundImageUploadDialog.close();
                }
              },
              {
                text : t('Delete'),
                class: 'button button--danger',
                click() {
                  const colorDefault = formElements.color._defaultValue;
                  storage.setSetting(colorFieldNameArr, colorDefault);
                  drawing.removeBackgroundColor();
                  drawing.removeBackgroundImage();
                  drawing.render();
                  backgroundImageUploadDialog.close();
                }
              }
            ]
          });
          backgroundImageUploadDialog.showModal();
        }
      },
      image          : {
        title: 'Image',
        icon : ['fas', 'fa-image'],
        align: 'left',
        buildFormElements() {
          return {
            _type       : 'file',
            _name       : 'file',
            _label      : 'Image',
            _description: 'Select image to upload. Accept: PNG, JPG, GIF',
            _accept     : ['image/png', 'image/jpeg', 'image/gif'],
            _required   : true
          };
        },
        click() {
          const { markup, drawing } = this;
          const scopeFn = Toolbar.toolbarElements.left.image;
          const formMarkup = markup.form(
            'image-upload-form',
            scopeFn.buildFormElements()
          );
          const { form, formData } = formMarkup;
          const imageUploadDialog = markup.dialog(form, {
            title  : t('Image'),
            buttons: [
              {
                text : t('Save'),
                class: 'button button--primary',
                click() {
                  if (!formMarkup.formValide()) {
                    return;
                  }
                  const reader = new FileReader();
                  const formDataValues = formData();
                  const file = formDataValues.get('file');
                  if (file.size) {
                    reader.onload = ({ target }) => {
                      drawing.setImage(target.result);
                    };
                    reader.readAsDataURL(file);
                  }
                  imageUploadDialog.close();
                }
              }
            ]
          });
          imageUploadDialog.showModal();
        }
      },
      spacer2        : {
        type : 'spacer',
        align: 'left'
      },
      textarea       : {
        title: 'Textarea',
        icon : ['fas', 'fa-paragraph'],
        align: 'left',
        click() {
          this.drawing.setTextbox('Text');
        }
      }
    },
    right: {
      devices   : {
        type  : 'devicesChanger',
        title : 'Devices',
        align : 'right',
        weight: -99,
        click({ target }) {
          const { devices, drawing } = this;
          const { device } = target.dataset;
          const { children } = target.parentNode;
          const isActiveClass = 'is-active';
          if (devices.change(device)) {
            Object.entries(children).forEach(([, elem]) => {
              elem.classList.remove(isActiveClass);
            });
            target.classList.add(isActiveClass);
            drawing.updateViewports();
          }
        }
      },
      spacer    : {
        type  : 'spacer',
        align : 'right',
        weight: -98
      },
      fullScreen: {
        title: 'Full screen',
        icon : ['fas', 'fa-expand'],
        align: 'right',
        click() {
          const { storage, drawing } = this;
          const className = 'full-screen';
          const { classList } = storage.container;
          if (classList.contains(className)) {
            storage.fullscreen = false;
            classList.remove(className);
          } else {
            storage.fullscreen = true;
            classList.add(className);
          }
          drawing.updateViewports();
        }
      },
      settings  : {
        title: 'Settings',
        icon : ['fas', 'fa-cog'],
        align: 'right',
        buildDeviceFormElement(name) {
          return {
            _button   : _strFirstLetterUc(name),
            breakpoint: {
              _type : 'textfield',
              _label: 'Breakpoint',
              _value: `devices.${name}.breakpoint`
            },
            height    : {
              _type    : 'textfield',
              _label   : 'Height',
              _value   : `devices.${name}.height`,
              _required: true
            },
            width     : {
              _type    : 'textfield',
              _label   : 'Width',
              _value   : `devices.${name}.width`,
              _required: true
            }
          };
        },
        click() {
          const { storage, devices, markup, drawing } = this;
          const { settingsForm } = BannerBuilder;
          const scopeFn = Toolbar.toolbarElements.right.settings;
          devices.forEach(({ name }) => {
            settingsForm.devices[name] = scopeFn.buildDeviceFormElement(name);
          });

          const formHandler = markup.form('settings-form', settingsForm);
          const { form, formData } = formHandler;
          const settingsDialog = markup.dialog(form, {
            title  : t('Settings'),
            buttons: [
              {
                text : t('Save'),
                class: 'button button--primary',
                click() {
                  if (!formHandler.formValide()) {
                    return;
                  }
                  formData().forEach((value, name) => {
                    storage.setSetting(_parseFormName(name), value);
                  });
                  settingsDialog.close();
                  drawing.updateViewports();
                }
              }
            ]
          });
          settingsDialog.showModal();
        }
      }
    }
  };

  /**
   * Banner builder drawing handler.
   *
   * @param {Object} self devices handler
   *
   * @constructor
   */
  function Drawing(self) {
    const { storage, devices, markup } = self;

    /**
     * Storage handler.
     *
     * @type {BannerBuilder.storage|Storage}
     */
    this.storage = storage;

    /**
     * Devices handler.
     *
     * @type {BannerBuilder.devices|Devices}
     */
    this.devices = devices;

    /**
     * Markup handler.
     *
     * @type {BannerBuilder.markup|Markup}
     */
    this.markup = markup;

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvasWrp = document.createElement('div');

    /**
     * Canvas object.
     *
     * @type {Object}
     */
    this.canvas = {};
  }

  /**
   * Build the canvas markup.
   *
   * @returns {HTMLDivElement} toolbar element
   */
  Drawing.prototype.build = function() {
    this.canvasWrp.classList.add('canvas-wrp');
    this.devices.forEach(({ name }) => {
      const canvasCn = document.createElement('div');
      const canvas = document.createElement('canvas');
      canvasCn.classList.add('canvas-item');
      canvas.id = `canvas-${name}-${_randStr()}`;
      this.canvas[name] = canvas;
      canvasCn.appendChild(canvas);
      this.canvasWrp.appendChild(canvasCn);
    });
    return this.canvasWrp;
  };

  /**
   * Build the canvas draw element.
   */
  Drawing.prototype.run = function() {
    Object.entries(this.canvas).forEach(([key, elem]) => {
      const preset = this.storage.canvas[key];
      const fabricCanvas = new fabric.Canvas(elem.id);
      preset && preset.objects.length && fabricCanvas.loadFromJSON(preset);
      this.storage.canvas[key] = fabricCanvas;
      this.updateViewport(key);
    });

    const { name } = this.devices.current;
    document.addEventListener('keydown', ({ key }) => {
      const draw = this.getDraw(name);
      const currentElem = draw.getActiveObject();
      if (currentElem) {
        switch (key) {
          case 'Delete':
            draw.remove(currentElem);
            break;
          default:
        }
      }
    });
  };

  /**
   * Get the canvas wrapper size.
   *
   * @return {{width: number, height: number}} size object
   *
   * @private
   */
  Drawing.prototype.getCanvasWrpSize = function() {
    const { innerHeight, innerWidth } = window;
    const { clientWidth, parentNode } = this.canvasWrp;
    const toolbarWrp = parentNode.querySelector('.toolbar-wrp');
    let height = Math.floor(clientWidth / (innerWidth / innerHeight));
    let maxHeight = Math.floor(innerHeight - toolbarWrp.clientHeight - 2);
    maxHeight = !this.storage.fullscreen ? maxHeight * 0.7 : maxHeight;
    height = height > maxHeight ? maxHeight : height;
    return {
      height,
      width: Math.floor(clientWidth)
    };
  };

  /**
   * Get a canvas object.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {HTMLCanvasElement|Object} canvas object
   */
  Drawing.prototype.getCanvas = function(deviceName = '') {
    deviceName = deviceName || this.devices.current.name;
    return this.canvas[deviceName] || {};
  };

  /**
   * Get a drawing object.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {Object} drawing object
   */
  Drawing.prototype.getDraw = function(deviceName = '') {
    deviceName = deviceName || this.devices.current.name;
    return this.storage.canvas[deviceName] || {};
  };

  /**
   * Update canvas element attributes.
   *
   * @param {string} deviceName canvas device name
   *
   * @private
   */
  Drawing.prototype.updateViewport = function(deviceName) {
    const { devices } = this;
    const { current } = devices;
    const isActiveClass = 'is-active';
    const canvas = this.getCanvas(deviceName);
    const canvasCurr = this.getCanvas(current.name);
    const canvasCn = canvas.closest('.canvas-item');
    const canvasCnItems = canvasCn.parentNode.children;
    const viewport = this.getCanvasWrpSize();
    let { height, width } = this.devices.find(deviceName);
    let breakpoint = `${viewport.width}px`;
    const scale = _scaleDimensions({
      height   : _convertToPixel(height, viewport.height),
      width    : _convertToPixel(width, viewport.width),
      maxHeight: viewport.height,
      maxWidth : viewport.width - 2
    });
    height = scale.height;
    width = scale.width;

    if (deviceName !== devices.last().name) {
      breakpoint = devices.next(deviceName).breakpoint;
    }

    Object.entries(canvasCnItems).forEach(([, elem]) => {
      elem.classList.remove(isActiveClass);
    });
    canvasCurr &&
    canvasCurr.closest('.canvas-item').classList.add(isActiveClass);

    if (deviceName === current.name) {
      this.canvasWrp.style.height = `${height + 1}px`;
    }

    canvasCn.style.height = `${height}px`;
    canvasCn.style.maxWidth = breakpoint;

    this.updateCanvas(deviceName, { height, width });
  };

  /**
   * Update all viewports.
   */
  Drawing.prototype.updateViewports = function() {
    this.devices.forEach(({ name }) => {
      this.updateViewport(name);
    });
  };

  /**
   * Update canvas data.
   *
   * @param {string} deviceName device name
   * @param {Object} opts options
   *
   * @private
   */
  Drawing.prototype.updateCanvas = function(deviceName, opts = {}) {
    const draw = this.getDraw(deviceName);
    const canvasCn = this.getCanvas(deviceName).parentNode;
    const { height, width } = opts;
    canvasCn.style.height = `${height}px`;
    canvasCn.style.width = `${width}px`;
    draw.setHeight(height);
    draw.setWidth(width);
  };

  /**
   * Render canvas.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.render = function(deviceName = '') {
    this.getDraw(deviceName).renderAll();
  };

  /**
   * Get background color.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {string} background color
   */
  Drawing.prototype.getBackgroundColor = function(deviceName = '') {
    return this.getDraw(deviceName).backgroundColor;
  };

  /**
   * Set background color.
   *
   * @param {string} color color
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setBackgroundColor = function(color, deviceName = '') {
    this.getDraw(deviceName).setBackgroundColor(color);
  };

  /**
   * Remove background color.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.removeBackgroundColor = function(deviceName = '') {
    this.setBackgroundColor(undefined, deviceName);
  };

  /**
   * Get background image element.
   *
   * @param {string} deviceName device name leave empty for current
   *
   * @return {*} background image element
   */
  Drawing.prototype.getBackgroundImage = function(deviceName = '') {
    return this.getDraw(deviceName).backgroundImage;
  };

  /**
   * Set background image element.
   *
   * @param {Object|string} src image src
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setBackgroundImage = function(
    src,
    opts = {},
    deviceName = ''
  ) {
    const draw = this.getDraw(deviceName);
    fabric.Image.fromURL(src, (img) => {
      const { scaleX, scaleY } = this._scaleImage(img, opts, draw);
      const position = this._positionImage(img, opts, draw);
      draw.setBackgroundImage(img, draw.renderAll.bind(draw), {
        ...opts,
        ...position,
        scaleX,
        scaleY
      });
    });
  };

  /**
   * Update background image element.
   *
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.updateBackgroundImage = function(
    { scale, originX, originY },
    deviceName = ''
  ) {
    const draw = this.getDraw(deviceName);
    const backgroundImage = this.getBackgroundImage(deviceName);

    if (!backgroundImage) {
      return;
    }

    if (backgroundImage.scale !== scale) {
      const { scaleX, scaleY } = this._scaleImage(
        backgroundImage,
        {
          scale,
          originX,
          originY
        },
        draw
      );
      backgroundImage.scale = scale;
      backgroundImage.scaleX = scaleX;
      backgroundImage.scaleY = scaleY;
    }

    if (
      backgroundImage.originX !== originX ||
      backgroundImage.originY !== originY
    ) {
      const position = this._positionImage(
        backgroundImage,
        {
          originX,
          originY
        },
        draw
      );
      backgroundImage.top = position.top;
      backgroundImage.left = position.left;
      backgroundImage.originX = position.originX;
      backgroundImage.originY = position.originY;
    }
  };

  /**
   * Update background image element.
   *
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.removeBackgroundImage = function(deviceName = '') {
    this.getDraw(deviceName).backgroundImage = undefined;
  };

  /**
   * Set image element.
   *
   * @param {Object|string} src image src
   * @param {Object} opts image object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setImage = function(src, opts = {}, deviceName = '') {
    const draw = this.getDraw(deviceName);
    fabric.Image.fromURL(src, (img) => {
      if (img.height > draw.height || img.width > draw.width) {
        const { scaleX, scaleY } = this._scaleImage(
          img,
          {
            scale: 'contain'
          },
          draw
        );
        img.scaleX = scaleX;
        img.scaleY = scaleY;
      }
      draw.centerObject(img);
      draw.add(img);
    });
  };

  /**
   * Set image element.
   *
   * @param {string} msg text content
   * @param {Object} opts options object
   * @param {string} deviceName device name leave empty for current
   */
  Drawing.prototype.setTextbox = function(
    msg = '',
    opts = {},
    deviceName = ''
  ) {
    const draw = this.getDraw(deviceName);
    const text = new fabric.Textbox(msg, opts);
    draw.centerObject(text);
    draw.add(text);
  };

  /**
   * Scale background image.
   *
   * @param {Object} data data object
   * @param {Object} opts options object
   * @param {Object} draw draw object
   *
   * @return {{scaleX: number, scaleY: number}} background image element
   *
   * @private
   */
  Drawing.prototype._scaleImage = function(
    { height, width },
    { scale, scaleX, scaleY },
    draw
  ) {
    let switchVar;
    switch (scale) {
      case 'cover':
        switchVar = _coverDimensions({
          height,
          width,
          maxHeight: draw.height,
          maxWidth : draw.width
        });
        scaleX = switchVar.ratio;
        scaleY = switchVar.ratio;
        break;
      case 'contain':
        switchVar = _scaleDimensions({
          height,
          width,
          maxHeight: draw.height,
          maxWidth : draw.width
        });
        scaleX = switchVar.ratio;
        scaleY = switchVar.ratio;
        break;
      default:
        scaleX = draw.width / width;
        scaleY = draw.height / height;
    }
    return { scaleX, scaleY };
  };

  /**
   * Position background image.
   *
   * @param {Object} data data object
   * @param {Object} opts options object
   * @param {Object} draw draw object
   *
   * @return {{top: number, left: number, originX: string, originY:
   *   string}} postion object
   *
   * @private
   */
  Drawing.prototype._positionImage = function(
    { height, width },
    { originX, originY },
    draw
  ) {
    let top;
    let left;
    switch (originY) {
      case 'top':
        top = 0;
        break;
      case 'bottom':
        top = draw.height;
        break;
      default:
        top = draw.height / 2;
    }
    switch (originX) {
      case 'left':
        left = 0;
        break;
      case 'right':
        left = draw.width;
        break;
      default:
        left = draw.width / 2;
    }
    return { top, left, originX, originY };
  };

  /**
   * Banner builder main.
   *
   * @param {HTMLElement} elem The element to attach the editor to
   * @param {string} format The text format for the editor
   *
   * @constructor
   */
  function BannerBuilder(elem, format) {
    /**
     * Storage object.
     *
     * @type {Object}
     */
    this.storage = new Storage(elem, format);

    /**
     * Devices handler.
     *
     * @type {Devices}
     */
    this.devices = new Devices(this.storage.settings.devices);

    /**
     * Markup handler.
     *
     * @type {Markup}
     */
    this.markup = new Markup(this);

    /**
     * Markup handler.
     *
     * @type {Drawing}
     */
    this.drawing = new Drawing(this);

    /**
     * Toolbar handler.
     *
     * @type {Toolbar}
     */
    this.toolbar = new Toolbar(this);

    // build markup
    const { element, container } = this.storage;
    container.classList.add('banner-builder');
    container.appendChild(this.toolbar.build());
    container.appendChild(this.drawing.build());
    element.parentNode.appendChild(container);

    // run drawing handler
    this.drawing.run();
  }

  /**
   * Detach banner builder event.
   *
   * @param {string} trigger The event trigger for the detach
   */
  BannerBuilder.prototype.detach = function(trigger) {
    const { element, container } = this.storage;
    if (trigger === 'serialize') {
      this.storage.save();
    } else {
      element.value = '';
      element.dataset.editorValueOriginal = '';
      element.style.display = 'block';
      container.style.display = 'none';
    }
  };

  /**
   * On change banner builder event.
   */
  BannerBuilder.prototype.onChange = function() {
    const { element, container } = this.storage;
    element.style.display = 'none';
    container.style.display = 'block';
  };

  /**
   * Banner builder default settings.
   */
  BannerBuilder.defSettings = {
    devices: {
      mobile : {
        breakpoint: '0',
        height    : '100vh',
        width     : '360px'
      },
      tablet : {
        breakpoint: '768px',
        height    : '100vh',
        width     : '1024px'
      },
      desktop: {
        breakpoint: '1024px',
        height    : '100vh',
        width     : '1920px'
      }
    }
  };

  /**
   * Banner builder settings form layout
   */
  BannerBuilder.settingsForm = {
    devices: {
      _type: 'tabs'
    }
  };

  /**
   * Drupal editor object.
   *
   * @type {*}
   */
  editors.banner_builder = {
    attach  : (element, format) => {
      if (!element.hasOwnProperty('BannerBuilder')) {
        element.BannerBuilder = new BannerBuilder(element, format);
      }
    },
    detach  : (element, format, trigger) => {
      element.hasOwnProperty('BannerBuilder') &&
      element.BannerBuilder.detach(trigger);
    },
    onChange: (element) => {
      element.hasOwnProperty('BannerBuilder') &&
      element.BannerBuilder.onChange();
    }
  };

  /**
   * Return a list off all available fonts and load custom fonts.
   *
   * @param {Object} format format settings
   *
   * @return {Array} available fonts
   */
  function _availableFonts(format) {
    const { fonts } = document;
    const { editorSettings } = format;
    const { featuresSettings } = editorSettings || {};
    const { defaultFonts, customFonts } = featuresSettings.fonts || {};
    const fontAvailable = [];
    // noinspection SpellCheckingInspection
    const fontCheck = new Set([
      // Windows 10
      'Arial',
      'Arial Black',
      'Bahnschrift',
      'Calibri',
      'Cambria',
      'Cambria Math',
      'Candara',
      'Comic Sans MS',
      'Consolas',
      'Constantia',
      'Corbel',
      'Courier New',
      'Ebrima',
      'Franklin Gothic Medium',
      'Gabriola',
      'Gadugi',
      'Georgia',
      'HoloLens MDL2 Assets',
      'Impact',
      'Ink Free',
      'Javanese Text',
      'Leelawadee UI',
      'Lucida Console',
      'Lucida Sans Unicode',
      'Malgun Gothic',
      'Marlett',
      'Microsoft Himalaya',
      'Microsoft JhengHei',
      'Microsoft New Tai Lue',
      'Microsoft PhagsPa',
      'Microsoft Sans Serif',
      'Microsoft Tai Le',
      'Microsoft YaHei',
      'Microsoft Yi Baiti',
      'MingLiU-ExtB',
      'Mongolian Baiti',
      'MS Gothic',
      'MV Boli',
      'Myanmar Text',
      'Nirmala UI',
      'Palatino Linotype',
      'Segoe MDL2 Assets',
      'Segoe Print',
      'Segoe Script',
      'Segoe UI',
      'Segoe UI Historic',
      'Segoe UI Emoji',
      'Segoe UI Symbol',
      'SimSun',
      'Sitka',
      'Sylfaen',
      'Symbol',
      'Tahoma',
      'Times New Roman',
      'Trebuchet MS',
      'Verdana',
      'Webdings',
      'Wingdings',
      'Yu Gothic',
      // macOS
      'American Typewriter',
      'Andale Mono',
      'Arial',
      'Arial Black',
      'Arial Narrow',
      'Arial Rounded MT Bold',
      'Arial Unicode MS',
      'Avenir',
      'Avenir Next',
      'Avenir Next Condensed',
      'Baskerville',
      'Big Caslon',
      'Bodoni 72',
      'Bodoni 72 Oldstyle',
      'Bodoni 72 Smallcaps',
      'Bradley Hand',
      'Brush Script MT',
      'Chalkboard',
      'Chalkboard SE',
      'Chalkduster',
      'Charter',
      'Cochin',
      'Comic Sans MS',
      'Copperplate',
      'Courier',
      'Courier New',
      'Didot',
      'DIN Alternate',
      'DIN Condensed',
      'Futura',
      'Geneva',
      'Georgia',
      'Gill Sans',
      'Helvetica',
      'Helvetica Neue',
      'Herculanum',
      'Hoefler Text',
      'Impact',
      'Lucida Grande',
      'Luminari',
      'Marker Felt',
      'Menlo',
      'Microsoft Sans Serif',
      'Monaco',
      'Noteworthy',
      'Optima',
      'Palatino',
      'Papyrus',
      'Phosphate',
      'Rockwell',
      'Savoye LET',
      'SignPainter',
      'Skia',
      'Snell Roundhand',
      'Tahoma',
      'Times',
      'Times New Roman',
      'Trattatello',
      'Trebuchet MS',
      'Verdana',
      'Zapfino'
    ]);

    if (defaultFonts) {
      fonts.ready.then(() => {
        fontCheck.forEach((font) => {
          if (fonts.check(`12px "${font}"`)) {
            fontAvailable.push(font);
          }
        });
      });
    }

    if (customFonts) {
      fontAvailable.push(...(customFonts.split(/\r?\n/) || []));
    }

    return fontAvailable.sort();
  }

  /**
   * Scale dimensions.
   *
   * @param {{width: number, height: number, maxHeight: number, maxWidth:
   *   number}} obj data object
   *
   * @return {{width: number, height: number, ratio: number}} scaled data
   *   object
   */
  function _scaleDimensions({ height, width, maxHeight, maxWidth }) {
    const ratio = Math.min(maxWidth / width, maxHeight / height);
    return {
      height: Math.floor(ratio * height),
      width : Math.floor(ratio * width),
      ratio
    };
  }

  /**
   * Crop dimensions.
   *
   * @param {{width: number, height: number, maxHeight: number, maxWidth:
   *   number}} obj data object
   *
   * @return {{width: number, height: number, ratio: number}} scaled data
   *   object
   */
  function _coverDimensions({ height, width, maxHeight, maxWidth }) {
    const ratio = Math.max(maxWidth / width, maxHeight / height);
    return {
      height: Math.floor(ratio * height),
      width : Math.floor(ratio * width),
      ratio
    };
  }

  /**
   * Connect css unit string to number.
   *
   * @param {string|number} val css unit string
   * @param {number} base calc base
   *
   * @return {number} pixel value
   */
  function _convertToPixel(val, base = 0) {
    if (typeof val === 'number') {
      return val;
    }

    const { number, unit } = _splitCssUnit(val);
    if (number) {
      switch (unit) {
        case '%':
        case 'vh':
        case 'vw':
          return base * (number / 100);
        default:
          return number;
      }
    }
    return 0;
  }

  /**
   * Split css unit into number and unit
   *
   * @param {string} str css unit string
   *
   * @return {{number: (number), unit: (string)}} object
   */
  function _splitCssUnit(str) {
    const regex = /(\d*)(.*)/gm;
    const m = regex.exec(str);
    const [, number, unit] = m;
    return { number: parseInt(number || '0', 10), unit: unit || '' };
  }

  /**
   * Merge two objects, second override first.
   *
   * @param {Object} objA first object
   * @param {Object} objB second object
   *
   * @return {Object} merged object
   */
  function _mergeObj(objA, objB) {
    Object.entries(objB).forEach((elem) => {
      const [p] = elem;
      try {
        if (typeof objB[p] === 'object') {
          objA[p] = _mergeObj(objA[p], objB[p]);
        } else {
          objA[p] = objB[p];
        }
      }
      catch (e) {
        objA[p] = objB[p];
      }
    });
    return objA;
  }

  /**
   * Clone an object and create a new reference.
   *
   * @param {Object} obj object to clone
   *
   * @return {Object} cloned object
   */
  function _cloneObj(obj) {
    return JSON.parse(JSON.stringify(obj || {})) || {};
  }

  /**
   * Remove property ky key recursively.
   *
   * @param {Object} obj object to clean
   */
  function _removePrivateProp(obj) {
    if (typeof obj === 'object') {
      Object.entries(obj).forEach(([prop]) => {
        if (prop[0] === '_') {
          delete obj[prop];
        } else if (typeof obj[prop] === 'object') {
          _removePrivateProp(obj[prop]);
        }
      });
    }
  }

  /**
   * Remove property ky key recursively and return a new object.
   *
   * @param {Object} obj object to clean.
   *
   * @return {Object} clean object
   */
  function _removePrivatePropNew(obj) {
    if (typeof obj !== 'object') {
      return {};
    }
    const ret = _cloneObj(obj);
    _removePrivateProp(ret);
    return ret;
  }

  /**
   * Build form name.
   *
   * @param {array} arr String to format
   *
   * @returns {string} form name string
   */
  function _buildFormName(arr = []) {
    const { length } = Object.keys(arr);
    if (length) {
      arr = _cloneObj(arr);
      const [key] = arr.splice(0, 1);
      if (length === 1) {
        return key || '';
      }
      return `${key || ''}[${arr.join('][')}]`;
    }
    return '';
  }

  /**
   * Parse form name.
   *
   * @param {string} str form name string
   *
   * @returns {array} name array
   */
  function _parseFormName(str) {
    const ret = [];
    const regex = /^([\w\d\-_]*)\[(.*)]/gm;
    const m = regex.exec(str) || [];
    const [first, second] = m.splice(1);
    ret.push(first || str);
    if (typeof second === 'string') {
      ret.push(...second.split(']['));
    }
    return ret;
  }

  /**
   * Set first letter to uppercase.
   * @param {string} str string
   *
   * @return {string} string
   */
  function _strFirstLetterUc(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * Return random string.
   *
   * @returns {string} random string
   */
  function _randStr() {
    return (Math.random() + 1).toString(36).substring(2);
  }
})(jQuery, Drupal, fabric);
