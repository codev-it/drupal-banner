// noinspection DuplicatedCode,JSUnresolvedReference

(($, Drupal, once) => {
  const { behaviors } = Drupal;

  /**
   * Banner builder svg resizer.
   *
   * @param {HTMLElement} context current context
   */
  Drupal.bannerBuilderSVGResizer = (context) => {
    const $elems = $(context).find('[data-banner-builder-svg]');
    $elems.each((i) => {
      const $elem = $elems.eq(i);
      const $children = $elem.children();
      if ($children.length > 1) {
        $children.each((elemI) => {
          const $item = $children.eq(elemI);
          const $nextItem = $children.eq(elemI + 1);
          const breakpoint = $item.data('banner-builder-svg-breakpoint');
          const mediaQuery = [];

          if (breakpoint) {
            mediaQuery.push(`(min-width: ${breakpoint})`);
          }

          if ($nextItem.length) {
            const nextBreakpoint = $nextItem.data(
              'banner-builder-svg-breakpoint'
            );
            nextBreakpoint && mediaQuery.push(`(max-width: ${nextBreakpoint})`);
          }

          $item.hide();
          if (window.matchMedia(mediaQuery.join(' and ')).matches) {
            $item.show();
          }
        });
      }
    });
  };

  /**
   * Banner builder svg resizer behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevBannerBuilderSvg = {
    attach: (context) => {
      if (!once('banner-builder-svg-resizer', $(context)).length) {
        return;
      }

      const $window = $(window);
      Drupal.bannerBuilderSVGResizer(context);
      $window.resize(() => {
        Drupal.bannerBuilderSVGResizer(context);
      });
    }
  };
})(jQuery, Drupal, once);
