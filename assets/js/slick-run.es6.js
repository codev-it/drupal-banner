// noinspection JSUnresolvedReference

/**
 * @file
 * Behaviors of codev_banner module.
 */
(($, Drupal, once) => {
  const { behaviors } = Drupal;

  behaviors.codevBannerSlick = {
    attach: (context) => {
      once('init-slick-slider', '[data-slick]', context).forEach((item) => {
        const $item = $(item);
        if ($item.children().length) {
          const options = $item.data('slick') || {};
          // noinspection JSUnresolvedFunction
          $item.slick(options);
        }
      });
    }
  };
})(jQuery, Drupal, once);
