<?php

use Drupal\codev_banner\Settings;
use Drupal\codev_utils\Helper\Field;
use Drupal\codev_utils\Helper\Media;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Markup;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_preprocess_banner().
 *
 * @noinspection PhpUnused
 */
function template_preprocess_banner(&$variables): void {
  $variables['attributes']['data-slick'] = Json::encode($variables['options']);
  $variables['attributes']['data-banner-view-mode'] = $variables['view_mode'];
  $is_background_image = in_array($variables['view_mode'], [
    'background_image',
    'banner_contain',
    'banner_cover',
  ]);
  foreach ($variables['items'] as &$item) {
    /** @var \Drupal\media\Entity\Media $media */
    $media = $item['#media'];
    $format = Field::fmtCleanValue('field_text', $media, NULL, TRUE, 'format');
    if ($is_background_image && $format !== Settings::EDITOR_FORMAT) {
      $image_id = Field::fmtCleanValue('field_media_image', $media, NULL, TRUE, 'target_id');
      $item['#attributes']['style'][] = sprintf('background-image: url("%s");',
        Media::getUrlByFileId($image_id, $variables['image_style']));
    }
  }
}

/**
 * Implements hook_preprocess_banner().
 *
 * @noinspection PhpUnused
 */
function template_preprocess_banner_builder_svg(&$variables): void {
  $variables['attributes']['class'][] = 'banner-builder-svg';
  $variables['attributes']['data-banner-builder-svg'] = '';
  foreach ($variables['items'] as &$item) {
    $attr = ['data-banner-builder-svg-breakpoint' => $item['breakpoint'] ?? ''];
    if (!empty($item['svg']) && $variables['background_image'] ?? FALSE) {
      /** @var Markup $svg */
      $svg = $item['svg'];
      $attr['style'][] = sprintf(
        "background-image: url('data:image/svg+xml;charset=UTF-8,%s');",
        $svg->__toString());
    }
    $item['attributes'] = new Attribute($attr);
  }
}
