<?php

use Drupal\codev_banner\Settings;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_banner.forms.inc
 * .
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_banner_form_media_banner_add_form_alter(&$form, FormStateInterface $form_state): void {
  if (Settings::get('banner_builder_enable')
    && empty($form['field_text']['widget'][0]['#default_value'])) {
    $form['field_text']['widget'][0]['#format'] = Settings::EDITOR_FORMAT;
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_banner_inline_entity_form_entity_form_alter(array &$entity_form, FormStateInterface &$form_state): void {
  if ($entity_form['#bundle'] == 'banner'
    && $entity_form['#entity_type'] == 'media'
    && Settings::get('banner_builder_enable')
    && empty($entity_form['field_text']['widget'][0]['#default_value'])) {
    $entity_form['field_text']['widget'][0]['#format'] = Settings::EDITOR_FORMAT;
  }
}

