<?php
/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */

namespace Drupal\codev_banner\Form;

use Drupal;
use Drupal\codev_banner\BannerFinderInterface;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AdminSettingsForm.php
 * .
 */

/**
 * Class AdminSettingsForm.
 *
 * @package      Drupal\codev_banner\Form
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * Banner field name
   */
  protected string $field_name = BannerFinderInterface::BANNER_FIELD_NAME;

  /**
   * @var ModuleHandler
   */
  protected ModuleHandler $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigFormBase|AdminSettingsForm {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codev_banner_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_banner.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var Config $config */
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    // create fieldset container
    $form['node_types'] = [
      '#type'   => 'fieldset',
      '#title'  => $this->t('Banner node types'),
      '#weight' => 0,
    ];

    $form['node_types']['types'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Include banner to node types:'),
      '#options'       => $this->buildNodeTypeOptions(),
      '#default_value' => Utils::getArrayValue('node_types', $data, []),
    ];

    $form['banner_builder'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Banner builder settings'),
      '#weight' => 0,
      '#open'   => TRUE,
    ];

    $form['banner_builder']['banner_builder_enable'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable banner builder by default.'),
      '#default_value' => $data['banner_builder_enable'] ?? TRUE,
    ];

    $user = Drupal::currentUser();
    if ($user->hasPermission('administer banner field group')) {
      if ($this->moduleHandler->moduleExists('field_group')) {
        $form['field_group'] = [
          '#type'   => 'details',
          '#title'  => $this->t('Node field groups settings'),
          '#weight' => 0,
        ];

        $form['field_group']['field_group_parent'] = [
          '#title'         => $this->t('Field group parent'),
          '#type'          => 'textfield',
          '#default_value' => !empty($data['field_group_parent'])
            ? $data['field_group_parent'] : '',
        ];

        $form['field_group']['field_group_type'] = [
          '#type'          => 'select',
          '#title'         => $this->t('Select field group type'),
          '#options'       => $this->buildFieldGroupTypes(),
          '#empty_option'  => $this->t('- Select a group type -'),
          '#default_value' => !empty($data['field_group_type'])
            ? $data['field_group_type'] : '',
          '#required'      => TRUE,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get node types option list
   */
  protected function buildNodeTypeOptions(): array {
    $ret = [];
    $exclude = [];
    /** @noinspection PhpUnhandledExceptionInspection */
    $node_types = Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    if ($this->moduleHandler->moduleExists('codev_pages')) {
      $exclude[] = 'layout_builder_global';
    }
    /** @var NodeType $item */
    foreach ($node_types as $item) {
      $key = $item->get('type');
      if (!in_array($key, $exclude)) {
        $ret[$key] = $item->label();
      }
    }
    return $ret;
  }

  /**
   * Get field group types option list
   */
  protected function buildFieldGroupTypes(): array {
    $ret = [];
    $field_group_module_path = $this->moduleHandler
      ->getModule('field_group')
      ->getPath();
    $schema_file = sprintf('%s/config/schema/%s.yml',
      $field_group_module_path,
      'field_group.field_group_formatter_plugin.schema');
    $schema = Yaml::decode(file_get_contents($schema_file));
    foreach (array_keys($schema) as $key) {
      $key = explode('.', $key);
      $last_key = end($key);
      $ret[$last_key] = $last_key;
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   *
   * @throws EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    // set types
    $types = [];
    foreach ($form_state->getValue('types') as $type) {
      if (!empty($type)) {
        $types[] = $type;
      }
    }
    $this->createFieldStorageIfNotExist();
    $this->addFieldToNodeTypes($types);
    $types_remove = !empty($data['node_types'])
      ? array_diff($data['node_types'], $types) : [];
    $this->removeFieldFromNodeTypes($types_remove);
    $config->set('node_types', $types)
      ->set('banner_builder_enable', $form_state->getValue('banner_builder_enable'))
      ->save();

    // set field groups
    if ($this->moduleHandler->moduleExists('field_group')) {
      $field_group_parent = $form_state->getValue('field_group_parent');
      $field_group_type = $form_state->getValue('field_group_type');
      if (!empty($field_group_parent) && !empty($field_group_type)) {
        $config->set('field_group_parent', $field_group_parent)
          ->set('field_group_type', $field_group_type)
          ->save();
      }
    }
  }

  /**
   * Create field storage
   */
  protected function createFieldStorageIfNotExist(): void {
    if (!FieldStorageConfig::loadByName('node', $this->field_name)) {
      /** @noinspection PhpUnhandledExceptionInspection */
      FieldStorageConfig::create([
        'entity_type' => 'node',
        'field_name'  => $this->field_name,
        'type'        => 'entity_reference',
        'settings'    => [
          'target_type' => 'media',
        ],
        'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      ])->save();
    }
  }

  /**
   * Add field to node type
   *
   * @param $types
   *   Node types id's.
   *
   * @throws EntityStorageException
   */
  protected function addFieldToNodeTypes($types): void {
    foreach ($types as $type) {
      if (!FieldConfig::loadByName('node', $type, $this->field_name)) {
        FieldConfig::create([
          'field_name'  => $this->field_name,
          'entity_type' => 'node',
          'bundle'      => $type,
          'label'       => 'Banner',
          'settings'    => [
            'handler_settings' => [
              'target_bundles' => [
                'banner' => 'banner',
              ],
            ],
          ],
        ])->save();
        $this->setFieldFormDisplay($type);
      }
    }
  }

  /**
   * Set node type form display
   *
   * @param $type
   *   Node type id.
   */
  protected function setFieldFormDisplay($type): void {
    $config_name = sprintf('core.entity_form_display.node.%s.default', $type);
    $config = $this->configFactory->getEditable($config_name);
    $data = $config->getRawData();
    if (!empty($data['hidden'][$this->field_name])) {
      unset($data['hidden'][$this->field_name]);
    }

    if (empty($data['hidden'][$this->field_name])) {
      $data['content'][$this->field_name] = [
        'type'                 => 'inline_entity_form_complex',
        'weight'               => '-99',
        'region'               => 'content',
        'settings'             => [
          'form_mode'       => 'default',
          'override_labels' => TRUE,
          'label_singular'  => 'Banner',
          'label_plural'    => "Banner's",
          'allow_new'       => TRUE,
          'allow_existing'  => TRUE,
          'allow_duplicate' => TRUE,
          'match_operator'  => 'CONTAINS',
          'collapsible'     => FALSE,
          'collapsed'       => FALSE,
        ],
        'third_party_settings' => [
          'entity_browser_entity_form' => [
            'entity_browser_id' => 'media_entity_browser_banner_modal',
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('field_group')) {
      $config_data = $this->config($this->getEditableConfigNames()[0])
        ->getRawData();
      if (!empty($data['third_party_settings']['field_group'][$config_data['field_group_parent']])) {
        $data['third_party_settings']['field_group'][$config_data['field_group_parent']]['children'][] = 'group_banner';
        $data['third_party_settings']['field_group']['group_banner'] = [
          'label'           => 'Banner',
          'parent_name'     => $config_data['field_group_parent'],
          'weight'          => 99,
          'format_type'     => $config_data['field_group_type'],
          'region'          => 'content',
          'children'        => [$this->field_name],
          'format_settings' => [
            'id'              => '',
            'classes'         => '',
            'description'     => '',
            'formatter'       => 'close',
            'required_fields' => FALSE,
          ],
        ];
      }
    }
    $config->setData($data)->save();
  }

  /**
   * Remove field from node type
   *
   * @param $types
   *   Node types id's.
   *
   * @throws EntityStorageException
   */
  protected function removeFieldFromNodeTypes($types): void {
    foreach ($types as $type) {
      /** @var FieldConfig $field_info */
      $field_info = FieldConfig::loadByName('node', $type, $this->field_name);
      if (!empty($field_info)) {
        $field_info->delete();
      }
    }
  }

}
