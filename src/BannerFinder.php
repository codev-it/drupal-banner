<?php

namespace Drupal\codev_banner;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: BannerFinder.php
 * .
 */

/**
 * Class BannerFinder.
 *
 * The default banner finder implementation.
 *
 * @package      Drupal\codev_banner
 *
 * @noinspection PhpUnused
 */
class BannerFinder implements BannerFinderInterface {

  /**
   * The active node.
   *
   * @var NodeInterface|null
   */
  protected ?NodeInterface $activeNode;

  /**
   * The request stack.
   *
   * @var RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a Bannerfinder object.
   *
   * @param RequestStack $requestStack
   *   The request stacks.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveNode(): ?NodeInterface {
    if (empty($this->activeNode)) {
      $this->activeNode = $this->requestStack->getCurrentRequest()->attributes->get('node');
    }
    return $this->activeNode;
  }

  /**
   * {@inheritdoc}
   */
  public function getBannerEntitiesFromActiveNode(): array {
    if ($node = $this->getActiveNode()) {
      return $this->getBannerEntitiesFromNode($node);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getBannerEntitiesFromNode(NodeInterface $node): array {
    if ($node->hasField(self::BANNER_FIELD_NAME)) {
      /** @var EntityReferenceFieldItemList $field */
      $field = $node->get(self::BANNER_FIELD_NAME);
      return $field->referencedEntities();
    }
    return [];
  }

}
