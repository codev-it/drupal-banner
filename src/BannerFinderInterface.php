<?php

namespace Drupal\codev_banner;

use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: BannerFinderInterface.php
 * .
 */

/**
 * Interface BannerFinderInterface
 *
 * The banner finder interface.
 *
 * @package Drupal\codev_banner
 */
interface BannerFinderInterface {

  /**
   * The banner field name id.
   */
  const BANNER_FIELD_NAME = 'field_banner';

  /**
   * Finds and returns the active node, if available.
   *
   * @return NodeInterface|null
   *   The active node - based on the active route context - or NULL, if not
   *   found (non-node routes).
   */
  public function getActiveNode(): ?NodeInterface;

  /**
   * Loads banner entity IDs from the active node (based on the route context).
   *
   * @return MediaInterface[]
   *   The banner entities.
   */
  public function getBannerEntitiesFromActiveNode(): array;

  /**
   * Loads banner entity IDs from the given node.
   *
   * @param NodeInterface $node
   *   The node which banner ids should be extracted.
   *
   * @return MediaInterface[]
   *   The banner entities.
   *
   * @noinspection PhpUnused
   */
  public function getBannerEntitiesFromNode(NodeInterface $node): array;

}
