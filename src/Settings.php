<?php

namespace Drupal\codev_banner;

use Drupal;
use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_banner
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * Module name.
   *
   * @var string
   */
  public const MODULE_NAME = 'codev_banner';

  /**
   * Banner builder editor format.
   *
   * @var string
   */
  public const EDITOR_FORMAT = 'banner_builder';

  /**
   * Return the default banner builder image folder.
   *
   * @return string
   */
  public static function getImageDir(): string {
    $default_scheme = Drupal::config('system.file')->get('default_scheme');
    return sprintf('%s://banner-builder', $default_scheme);
  }

  /**
   * Build the image path by the current default folder.
   *
   * @param string $name
   *
   * @return string
   */
  public static function buildImagePath(string $name): string {
    return sprintf('%s/%s', static::getImageDir(), $name);
  }

}
