<?php

namespace Drupal\codev_banner\Plugin\Editor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\editor\Plugin\EditorBase;

/**
 * Defines the "banner_builder" editor.
 *
 * @Editor(
 *   id = "banner_builder",
 *   label = @Translation("Banner builder"),
 *   supports_content_filtering = FALSE,
 *   supports_inline_editing = FALSE,
 *   is_xss_safe = FALSE,
 *   supported_element_types = {
 *     "textarea"
 *   }
 * )
 *
 * @noinspection PhpUnused
 */
class BannerBuilder extends EditorBase {

  /**
   * {@inheritDoc}
   */
  public function getDefaultSettings(): array {
    $settings = parent::getDefaultSettings();

    $settings['devices'] = [
      'mobile'  =>
        [
          'breakpoint' => '0',
          'height'     => '100vh',
          'width'      => '360px',
        ],
      'tablet'  =>
        [
          'breakpoint' => '768px',
          'height'     => '100vh',
          'width'      => '1024px',
        ],
      'desktop' =>
        [
          'breakpoint' => '1024px',
          'height'     => '100vh',
          'width'      => '1920px',
        ],
    ];

    $settings['features'] = [
      'background_image' => TRUE,
    ];

    return $settings;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $editor = $form_state->get('editor');
    $settings = $editor->getSettings();

    $form['features'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Features:'),
      '#options'       => [
        'backgroundImage' => $this->t('Background image'),
        'image'           => $this->t('Image'),
        'textarea'        => $this->t('Textarea'),
      ],
      '#default_value' => array_keys(array_filter($settings['features'] ?? [])),
    ];

    $form['features_settings_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['featuresSettings'] = ['#tree' => TRUE];

    $form['featuresSettings']['fonts'] = [
      '#type'  => 'details',
      '#title' => $this->t('Fonts'),
      '#group' => 'editor][settings][features_settings_tabs',
    ];

    $form['featuresSettings']['fonts']['defaultFonts'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default fonts'),
      '#description'   => $this->t('Include all system fonts.'),
      '#default_value' => $settings['featuresSettings']['fonts']['defaultFonts'] ?? TRUE,
    ];

    $form['featuresSettings']['fonts']['customFonts'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Custom fonts'),
      '#description'   => $this->t('A list of additional fonts offered in the "Font" drop-down menu. One font can be entered per line.'),
      '#default_value' => $settings['featuresSettings']['fonts']['customFonts'] ?? '',
    ];

    $form['devices'] = [
      '#type'  => 'fieldgroup',
      '#title' => $this->t('Devices defaults settings:'),
    ];

    foreach ($settings['devices'] as $key => $device) {
      $form['devices'][$key] = $this->breakpointFieldGroupMarkup($key, $device);
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $features = $form_state->getValue('features');
    foreach ($features as &$feature) {
      $feature = !empty($feature);
    }

    $form_state->setValue('features', $features);
    $form_state->unsetValue('features_settings_tabs');
  }

  /**
   * {@inheritDoc}
   */
  public function getJSSettings(Editor $editor): array {
    return $editor->getSettings() ?: [];
  }

  /**
   * {@inheritDoc}
   */
  public function getLibraries(Editor $editor): array {
    return ['codev_banner/banner-builder'];
  }

  /**
   * Return breakpoint field markup.
   *
   * @param $name
   * @param $data
   *
   * @return array[]
   */
  private function breakpointFieldGroupMarkup($name, $data): array {
    $ret = [
      '#type'  => 'details',
      '#title' => $name,
    ];
    foreach ($data as $key => $val) {
      $ret[$key] = [
        '#type'          => 'textfield',
        '#title'         => $this->t(ucfirst($key)),
        '#default_value' => $val ?: '',
      ];
    }
    return $ret;
  }

}
