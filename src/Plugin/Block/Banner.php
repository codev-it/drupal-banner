<?php

namespace Drupal\codev_banner\Plugin\Block;

use Drupal\codev_banner\BannerFinderInterface;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Theme\ThemeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Banner.php
 * .
 */

/**
 * Class Banner.
 *
 * Provides banner block.
 *
 * @Block(
 *   id = "banner_slider",
 *   admin_label = @Translation("Banner slider"),
 *   category = @Translation("Banner")
 * )
 *
 * @package      Drupal\codev_banner\Plugin\Block
 *
 * @noinspection PhpUnused
 */
class Banner extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The banner finder.
   *
   * @var BannerFinderInterface
   */
  protected BannerFinderInterface $bannerFinder;

  /**
   * The entity display repository.
   *
   * @var EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The entity repository.
   *
   * @var EntityRepository
   */
  protected EntityRepository $entityRepository;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler.
   *
   * @var ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The module handler.
   *
   * @var ThemeManager
   */
  protected ThemeManager $themeHandler;

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpFieldAssignmentTypeMismatchInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Banner|ContainerFactoryPluginInterface|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->bannerFinder = $container->get('codev_banner.banner_finder');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->themeHandler = $container->get('theme.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'view_mode'   => 'banner_cover',
      'image_style' => 'banner',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['view_mode'] = [
      '#type'          => 'select',
      '#title'         => $this->t('View mode'),
      '#description'   => $this->t('The view mode that will be used for rendering the banner in the block.'),
      '#default_value' => $this->configuration['view_mode'],
      '#options'       => $this->buildAvailableViewModes(),
    ];
    $form['image_style'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Image Style'),
      '#description'   => $this->t('Sets the image style, for the active block.'),
      '#default_value' => $this->configuration['image_style'],
      '#options'       => $this->buildAvailableImageStyles(),
    ];
    return $form;
  }

  /**
   * Gets available view modes of banner entities for block form configuration.
   */
  protected function buildAvailableViewModes(): array {
    $options = [];
    $media_modes = $this->entityDisplayRepository->getViewModes('media');
    foreach ($media_modes as $id => $info) {
      $options[$id] = $info['label'];
    }
    return $options;
  }

  /**
   * Gets available view modes of banner entities for block form configuration.
   */
  protected function buildAvailableImageStyles(): array {
    $options = [];
    /** @noinspection PhpUnhandledExceptionInspection */
    $image_style = $this->entityTypeManager
      ->getStorage('image_style')
      ->loadMultiple();
    foreach (array_keys($image_style) as $info) {
      $options[$info] = $info;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
    $this->configuration['image_style'] = $form_state->getValue('image_style');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $items = $this->bannerFinder->getBannerEntitiesFromActiveNode();
    $view_builder = $this->entityTypeManager->getViewBuilder('media');
    $view_mode = $this->configuration['view_mode'];

    foreach ($items as &$item) {
      $item = $view_builder->view($item, $view_mode);
    }

    if (!empty($items)) {
      $name = $this->getMachineNameSuggestion();

      $build['#theme'] = 'banner';
      $build['#items'] = $items;
      $build['#name'] = $name;
      $build['#bundle'] = $this->bannerFinder->getActiveNode()->bundle();
      $build['#view_mode'] = $this->configuration['view_mode'];
      $build['#image_style'] = $this->configuration['image_style'];
      $build['#options'] = $this->getSlickSliderOptions();

      $build['#attached']['library'][] = 'codev_banner/slick-slider';
    }
    return $build;
  }

  /**
   * Get slick slider options
   */
  protected function getSlickSliderOptions(): array {
    $options = [];
    $name = $this->getMachineNameSuggestion();
    $bundle = $this->bannerFinder->getActiveNode()->bundle();
    $view_mode = $this->configuration['view_mode'];
    $alters = [
      'banner_options',
      'banner_options_' . $name,
      'banner_options_' . $bundle,
      'banner_options_' . $view_mode,
      sprintf('banner_options_%s__%s', $name, $view_mode),
      sprintf('banner_options_%s__%s', $bundle, $view_mode),
      sprintf('banner_options_%s__%s__%s', $name, $bundle, $view_mode),
    ];
    foreach ($alters as $alter) {
      $this->themeHandler->alter($alter, $options);
      $this->moduleHandler->alter($alter, $options);
    }
    return $options;
  }

  /**
   * @inheritDoc
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * @inheritDoc
   */
  public function getCacheTags(): array {
    $tags = parent::getCacheTags();
    if ($node = $this->bannerFinder->getActiveNode()) {
      $tags = Cache::mergeTags($tags, ['node:' . $node->id()]);
    }
    return $tags;
  }

}
