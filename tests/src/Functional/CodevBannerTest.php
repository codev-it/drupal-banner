<?php

/** @noinspection PhpUnused */

namespace Drupal\Tests\codev_banner\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\media\Entity\Media;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevBannerTest.php
 * .
 */

/**
 * Class CodevBannerTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_banner\Functional
 *
 * @group        codev_banner
 *
 * @noinspection PhpUnused
 */
class CodevBannerTest extends FunctionalTestBase {

  /**
   * The admin user used for testing.
   *
   * @var User|UserInterface|false
   *   A user entity with administrative permissions for testing.
   */
  protected User|UserInterface|false $adminUser;

  /**
   * @var ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $this->configFactory = $this->container->get('config.factory');
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer banner',
      'administer blocks',
    ]);
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);
  }

  /**
   * Tests the functionality.
   *
   * @throws EntityStorageException
   * @throws ExpectationException
   */
  public function testCodevBanner() {
    // Check admin form and field exist.
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin/config/content/codev_banner');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Banner node types'));
    $this->assertSession()
      ->pageTextContains(t('Include banner to node types:'));
    $this->assertSession()->pageTextContains(t('Node field groups settings'));
    $this->assertSession()->pageTextContains(t('Field group parent'));
    $this->assertSession()->pageTextContains(t('Select field group type'));
    $this->submitForm([
      'types[page]' => 1,
    ], t('Save configuration'));

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/codev_banner');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Banner node types'));
    $this->assertSession()
      ->pageTextContains(t('Include banner to node types:'));
    $this->assertSession()
      ->pageTextNotContains(t('Node field groups settings'));
    $this->assertSession()->pageTextNotContains(t('Field group parent'));
    $this->assertSession()->pageTextNotContains(t('Select field group type'));
    $this->submitForm([
      'types[page]' => 1,
    ], t('Save configuration'));

    // Check block form and field exist.
    $this->drupalGet('admin/structure/block/add/banner_slider');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('View mode'));
    $this->assertSession()->pageTextContains(t('Image Style'));

    // Create banner slider block
    $this->submitForm([
      'id'                      => 'banner_slider',
      'settings[label]'         => t('Banner slider'),
      'settings[label_display]' => TRUE,
      'settings[view_mode]'     => 'background_image',
      'settings[image_style]'   => 'banner',
    ], t('Save block'));
    $this->drupalPlaceBlock('banner_slider', [
      'label'       => t('Banner slider'),
      'view_mode'   => 'background_image',
      'image_style' => 'banner',
    ]);

    // test banner field and node create
    $this->rebuildAll();
    $media = Media::create(['bundle' => 'banner']);
    $media->save();
    $node = $this->drupalCreateNode([
      'field_banner' => [
        'target_id' => $media->id(),
      ],
    ]);
    $node_fields = array_keys($node->getFields());
    $node_fields_definit = array_keys($node->getFieldDefinitions());
    $this->assertTrue(in_array('field_banner', $node_fields));
    $this->assertTrue(in_array('field_banner', $node_fields_definit));

    // anonymous tests
    $this->drupalLogout();
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet(sprintf('node/%s', $node->id()));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Banner slider'));

    $this->drupalGet('admin/config/content/codev_banner');
    $this->assertSession()->statusCodeEquals(403);
  }

}
